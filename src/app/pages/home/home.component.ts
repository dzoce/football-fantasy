import { TeamService } from './../../services/team.service';
import { Router, NavigationStart, NavigationEnd, RoutesRecognized, ActivationStart, ActivationEnd } from '@angular/router';
import { UserService } from './../../services/user.service';
import { Component, TemplateRef, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Ng4TwitterTimelineService } from 'ng4-twitter-timeline/lib/index';


@Component({
encapsulation: ViewEncapsulation.None,
  selector: 'ff-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  topFivePlayerList: any;
  divOneList: any;
  divTwoList: any;
   modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private ng4TwitterTimelineService: Ng4TwitterTimelineService,
    private userService: UserService,
    private teamService: TeamService,
    private router: Router
  ) {}

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  ngOnInit() {
    if (localStorage.getItem('ff-token') && parseFloat(localStorage.getItem('ff-hasSquad')) === 1) {
      this.router.navigate(['/dashboard']);
    }
    else if (parseFloat(localStorage.getItem('ff-hasSquad')) === 0) {
      this.router.navigate(['/squad-setup']);
    }
    else {
      this.getTopFivePlayers();
      this.getTopFiveDivone();
      this.getTopFiveDivtwo();
    }
  }
  closeModal() {
    this.modalRef.hide();
  }
  getTopFivePlayers() {
    this.userService.getTopFivePlayers()
    .subscribe(res => {
      console.log(res.data);
      this.topFivePlayerList = res.data;
    });
  }
  getTopFiveDivone() {
    this.userService.getTopFivePlayersDivisionOne()
    .subscribe(res => {
      console.log(res);
      this.divOneList = res.data;
    });
  }
  getTopFiveDivtwo() {
    this.userService.getTopFivePlayersDivisionTwo()
    .subscribe(res => {
      console.log(res);
      this.divTwoList = res.data;
    });
  }

}
