import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../services/team.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'ff-transfers',
  templateUrl: './transfers.component.html',
  styleUrls: ['./transfers.component.scss']
})
export class TransfersComponent implements OnInit {
  balance = 0;
  buttonText = 'Update Team';
  points: number;
  selectedPlayers = [];
  leagueID = localStorage.getItem('ff-league');
  positionsCounter = {
    GK: 0,
    DEF: 0,
    MID: 0,
    ATK: 0
  };
  makeTransfer = {
    l_id: this.leagueID,
    buy: [],
    sell: [],
    money: 0
  };

  constructor(
    private teamService: TeamService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.teamService.getMyTeam({l_id: this.leagueID}).subscribe(result => {
      this.selectedPlayers = _.concat(result.data.players.starting, result.data.players.subs);
      this.balance = parseFloat(result.data.meta.money);
      this.points = parseFloat(result.data.meta.points);
    });
  }

  updateTeam(event) {
    event.buy.forEach(player => {
      this.makeTransfer.buy.push(player.id);
    });
    event.sell.forEach(player => {
      this.makeTransfer.sell.push(player.id);
    });
    this.makeTransfer.money = event.money;

    this.teamService.makeTransfer(this.makeTransfer).subscribe(result => {
      setTimeout(() => {
        this.toastr.success('Transfer completed!', 'Success');
        this.router.navigate(['/my-team']);
      }, 50);
    });
  }

}
