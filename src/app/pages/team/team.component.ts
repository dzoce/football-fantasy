import { UserService } from './../../services/user.service';
import { FieldPosition } from './../../models/FieldPosition';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { BsModalService } from 'ngx-bootstrap/modal';

import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Player } from '../../models/Player';
import * as _ from 'lodash';
import * as moment from 'moment';
import { TeamService } from '../../services/team.service';
import { Formation } from '../../models/Formation';
import { SquadService } from '../../services/squad.service';

@Component({
  selector: 'ff-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  // playerDetailsId: number = null;
  playerDetailsModal = {
    id: 0,
    first_name: '',
    last_name: ''
  };
  availableFormations: Array<Formation>;
  selectedFormation: Formation = new Formation();
  captain: Array<number> = [];
  captainId: any;
  nextRound: any;
  userMeta = {user: {}, meta: {}, squadValue: 0};
  startTeam = [];
  substitutions = [];
  selectedTeam = [];
  leagueID = localStorage.getItem('ff-league');
  deadline: any;
  modalRef: BsModalRef;
  playerInfo: any;

  constructor(
    private dragula: DragulaService,
    private teamService: TeamService,
    private squadService: SquadService,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private userService: UserService
  ) {
    this.availableFormations = this.teamService.formations;
    this.dragula.drop.subscribe((value, second) => {
      this.teamService.selectedTeam = this.selectedTeam;

      if (value[2].title && value[3].title) {
        const positionFrom = this.teamService.selectedTeam[_.findIndex(this.teamService.selectedTeam, {'position': value[3].title})];
        const positionTo = this.teamService.selectedTeam[_.findIndex(this.teamService.selectedTeam, {'position': value[2].title})];

        if (positionTo.data.length > 0) {
          setTimeout(() => {
            const fromPlayerPosition = positionTo.data[1].position;
            const toPlayerPosition = positionTo.data[0].position;

            if (fromPlayerPosition === 'GK' && toPlayerPosition !== 'GK') {
              this.preventShift(positionFrom, positionTo);
              return false;
            }
            else if (fromPlayerPosition === 'DEF' && this.teamService.defPositions.indexOf(positionTo.position) === -1) {
              this.preventShift(positionFrom, positionTo);
              return false;
            }
            else if (fromPlayerPosition === 'MID' && this.teamService.midPositions.indexOf(positionTo.position) === -1) {
              this.preventShift(positionFrom, positionTo);
              return false;
            }
            else if (fromPlayerPosition === 'ATK' && this.teamService.forwardPositions.indexOf(positionTo.position) === -1) {
              this.preventShift(positionFrom, positionTo);
              return false;
            }
            else {
              this.shiftPlayers(positionFrom, positionTo);
            }
          }, 50);
        }
      }
      else if (value[1].title && value[2].title) {
        const draggedPlayerPosition = value[1].title;
        const fieldPosition = value[2].title;
        const fieldPositionIndex = _.findIndex(this.selectedTeam, { 'position': fieldPosition });
        const positionOnFieldGroup = this.selectedTeam[fieldPositionIndex];

        if (positionOnFieldGroup.data.length > 0) {
          if (draggedPlayerPosition === 'GK' && fieldPosition !== 'GK') {
            this.removeNonValidPlayer(positionOnFieldGroup, 'GK');
          }
          else if (draggedPlayerPosition === 'DEF' && this.teamService.defPositions.indexOf(fieldPosition) === -1) {
            this.removeNonValidPlayer(positionOnFieldGroup, 'DEF');
          }
          else if (draggedPlayerPosition === 'MID' && this.teamService.midPositions.indexOf(fieldPosition) === -1) {
            this.removeNonValidPlayer(positionOnFieldGroup, 'MID');
          }
          else if (draggedPlayerPosition === 'ATK' && this.teamService.forwardPositions.indexOf(fieldPosition) === -1) {
            this.removeNonValidPlayer(positionOnFieldGroup, 'ATK');
          }
          else {
            this.substitutePlayer(positionOnFieldGroup, fieldPosition, draggedPlayerPosition);
          }
        }
      }
      else if (value[2].title === '') {
        if (value[3].title === 'GK') {
          setTimeout(() => {
            const fieldPosition = value[3].title;
            const fieldPositionIndex = _.findIndex(this.selectedTeam, { 'position': fieldPosition });
            const draggedPlayerID = parseFloat(value[4].getAttribute('id'));
            const draggedPlayer = _.find(this.substitutions, ['id', draggedPlayerID]);
            this.selectedTeam[fieldPositionIndex].data.push(draggedPlayer);
            this.substitutions.splice((_.findIndex(this.substitutions, ['id', draggedPlayerID])), 1);
          }, 0);
        }
        setTimeout(() => {
            this.sortSubstitutions();
        }, 0);
      }
    });
  }

  ngOnInit() {
    this.availableFormations = this.teamService.formations;
    this.initTeamPage();
    console.log(this.captainId);
  }

  updateTeam() {
    const data = {
      selected_team: _.map(this.selectedTeam, 'data[0].id'),
      substitutions: _.map(this.substitutions, 'id'),
      captain: this.captain,
      formation: this.selectedFormation.name,
      l_id: this.leagueID
    };

    this.squadService.updateSquad(data).subscribe(result => {
      if (result.status === 200) {
        this.toastr.success('Team updated successfully.', 'Update Successful');
        this.initTeamPage();
      }
      else {
        this.toastr.error('An error occured while updating your team', 'Update failed');
      }
    });
  }

  private preventShift(from, to) {
    from.data.unshift(to.data[1]);
    to.data.splice(1, 1);
    from.data.splice(1, 1);
    this.toastr.error('These players have different possitions!', 'Change not allowed');
  }

  private shiftPlayers(from, to) {
    from.data.unshift(to.data[0]);
    to.data.splice(0, 1);
    from.data.splice(1, 1);
  }

  private substitutePlayer(position, fieldPosition, draggedPlayerPosition) {
    setTimeout(() => {
      if (position.position === fieldPosition && position.data.length > 1) {
        if (draggedPlayerPosition === 'GK' && fieldPosition === 'GK') {
          this.substitutions.splice(0, 0, position.data[0]);
          position.data.splice(0, 1);
        }
        else {
          this.substitutions.splice(1, 0, position.data[0]);
          position.data.splice(0, 1);
        }
        this.sortSubstitutions();
      }
    }, 50);
  }

  private removeNonValidPlayer(position, positionToKeep) {
    const playerToKeep = _.findIndex(position.data, {'position': positionToKeep});
    setTimeout(() => {
      if (position.data.length > 1) {
        this.substitutions.splice(1, 0, position.data[playerToKeep]);
        position.data.splice(playerToKeep, 1);
      }
      else {
        this.substitutions.splice(1, 0, position.data[0]);
        position.data.splice(0, 1);
      }
      this.toastr.error('Player cannot play on this position!', 'Position not valid');
      this.sortSubstitutions();
    }, 50);
  }

  private sortSubstitutions() {
    this.substitutions = _.sortBy(this.substitutions, (item) => {
      return [
        item.position === 'ATK',
        item.position === 'MID',
        item.position === 'DEF',
        item.position === 'GK'
      ];
    });
  }

  updateFormation() {
    this.teamService.selectedFormation = this.selectedFormation;
    this.setStartTeamAndSubs(this.selectedFormation.name);
  }

  setCaptain(id) {
    this.captain[0] = id;
    this.captainId = id;
    this.modalRef.hide();
  }

  playerDetails(player, modalTemplate: TemplateRef<any>) {
    this.modalRef = this.modalService.show(modalTemplate);
    this.playerInfo = player;
    console.log(this.playerInfo);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
    this.modalRef.hide();
  }

  initTeamPage() {
    this.teamService.getMyTeam({l_id: this.leagueID}).subscribe(result => {
      this.teamService.availablePlayers = _.concat(result.data.players.starting, result.data.players.subs);
      console.log(result.data);
      this.userMeta = {
        user: result.data.user,
        meta: result.data.meta,
        squadValue: _.reduce(_.map(this.teamService.availablePlayers, 'price'), (sum, n) => {
          return parseFloat(sum) + parseFloat(n);
        }, 0)
      };
      this.deadline = moment(result.data.deadline).format('MMMM D, YYYY');
      this.captain[0] = result.data.team.captain_id;
      this.captainId = result.data.team.captain_id;
      this.selectedFormation = _.filter(this.availableFormations, {'name': result.data.team.formation})[0];
      this.setStartTeamAndSubs(this.selectedFormation.name);
      this.getNextRound();
    });
  }

  getNextRound() {
    this.userService.getNextRound({l_id: this.leagueID}).subscribe(result => {
      this.nextRound = result.data;
    });
  }

  private setStartTeamAndSubs(formation) {
    const selectedFormationArray = formation.split('-');

    this.teamService.availablePlayers.forEach(player => {
      player.club.name.replace(' ', '_');
    });

    const goalkeeper = _.filter(this.teamService.availablePlayers, {position: 'GK'});
    const defenders = _.filter(this.teamService.availablePlayers, {position: 'DEF'});
    const midfielders = _.filter(this.teamService.availablePlayers, {position: 'MID'});
    const forwards = _.filter(this.teamService.availablePlayers, {position: 'ATK'});

    this.startTeam = _.concat(
      _.take(goalkeeper, 1),
      _.take(defenders, selectedFormationArray[0]),
      _.take(midfielders, selectedFormationArray[1]),
      _.take(forwards, selectedFormationArray[2])
    );

    this.substitutions = _.concat(
      _.drop(goalkeeper, 1),
      _.drop(defenders, selectedFormationArray[0]),
      _.drop(midfielders, selectedFormationArray[1]),
      _.drop(forwards, selectedFormationArray[2])
    );

    this.selectedTeam[0] = new FieldPosition('GK', [this.startTeam[0]]);

    this.selectedFormation.positions.forEach((position, index) => {
      this.selectedTeam[index + 1] = new FieldPosition(position, [this.startTeam[index + 1]]);
    });
  }
}
