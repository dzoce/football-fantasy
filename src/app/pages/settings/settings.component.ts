import { countries } from './../../shared/data/countries';
import { UserService } from './../../services/user.service';
import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as moment from 'moment';

@Component({
  selector: 'ff-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('ff-user'));
  countries = countries;
  myAccount: any = this.user;
  birthday: any;

  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getUserSettings().subscribe(result => {
      this.myAccount = result.data;
    });
  }

  updateUserSettings() {
    this.userService.updateUserSettings(this.myAccount).subscribe(result => {
      console.log(result);
      this.modalRef.hide();
    });
  }

  updateBirthday(event) {
    setTimeout(() => {
      if (event !== null) {
        this.myAccount.birthdate = moment(event).format('YYYY-MM-DD');
        console.log(this.myAccount.birthdate);
      }
    }, 0);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
    this.modalRef.hide();
  }
}
