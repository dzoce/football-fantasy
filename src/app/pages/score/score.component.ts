import { Router } from '@angular/router';
import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { UserService } from '../../services/user.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'ff-score',
    templateUrl: './score.component.html',
    styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {
    leagueID = localStorage.getItem('ff-league');
    recordsPerPage = '10';
    page = 1;
    searchTerm = '';
    modalRef: BsModalRef;
    topManagerList: any;
    selectedManager: any = {};

    constructor(
        private modalService: BsModalService,
        private userService: UserService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getUsers();
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    closeModal() {
        this.modalRef.hide();
    }

    getUsers() {
        const data = {
            l_id: this.leagueID,
            per_page: this.recordsPerPage,
            term: this.searchTerm
        };

        this.userService.getUsers(data, this.page).subscribe(res => {
          this.topManagerList = res.data;

          this.topManagerList.data.forEach(manager => {
              manager['startDate'] = moment(manager.created_at).format('DD-MM-YYYY');
          });
        });
    }

    viewTeamForManager() {
        this.modalRef.hide();
        this.router.navigate([`/view-team/${this.selectedManager.uuid}`]);
    }

    managerDetails(manager, modalTemplate: TemplateRef<any>) {
        this.modalRef = this.modalService.show(modalTemplate);
        this.selectedManager = manager;
    }

    nextPage(event) {
        this.page = event;
        this.getUsers();
    }

    previousPage(event) {
        this.page = event;
        this.getUsers();
    }

    setPage(event) {
        this.page = event;
        this.getUsers();
    }

    resetPage() {
        this.page = 1;
    }
}
