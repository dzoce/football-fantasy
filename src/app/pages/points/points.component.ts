import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@Component({
  selector: 'ff-points',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.scss']
})
export class PointsComponent implements OnInit {
  points: any;
  leagueID = localStorage.getItem('ff-league');

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAllPoints({l_id: this.leagueID}).subscribe(result => {
      this.points = result.data;
      console.log(result);
    });
  }

}
