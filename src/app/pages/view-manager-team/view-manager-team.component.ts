import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SquadService } from '../../services/squad.service';
import { TeamService } from '../../services/team.service';
import { Formation } from '../../models/Formation';
import * as _ from 'lodash';

@Component({
  selector: 'ff-view-manager-team',
  templateUrl: './view-manager-team.component.html',
  styleUrls: ['./view-manager-team.component.scss']
})
export class ViewManagerTeamComponent implements OnInit {
  managerUuid: string;
  managerData: any;
  selectedFormation: Formation;
  managerFullTeam: any;
  leagueID = localStorage.getItem('ff-league');

  constructor(
    private route: ActivatedRoute,
    private squadService: SquadService,
    private teamService: TeamService
  ) {
    this.route.params.subscribe(params => {
      this.managerUuid = params.managerUuid;
      console.log(this.managerUuid);
    });
  }

  ngOnInit() {
    this.squadService.getUserSquad({l_id: this.leagueID, uuid: this.managerUuid}).subscribe(result => {
      this.managerData = result.data;
      this.managerFullTeam = _.concat(result.data.players.starting, result.data.players.subs);
      this.selectedFormation = _.filter(this.teamService.formations, {'name': this.managerData.squad.formation})[0];
      this.managerData.squad.value = _.reduce(_.map(this.managerFullTeam, 'price'), (sum, n) => {
        return parseFloat(sum) + parseFloat(n);
      }, 0);
      this.managerData.players.starting.forEach((player, index) => {
        if (index !== 0) {
          this.managerData.players.starting[index].position = this.selectedFormation.positions[index - 1];
        }
      });
      console.log(result);
    });
  }

}
