import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManagerTeamComponent } from './view-manager-team.component';

describe('ViewManagerTeamComponent', () => {
  let component: ViewManagerTeamComponent;
  let fixture: ComponentFixture<ViewManagerTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewManagerTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManagerTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
