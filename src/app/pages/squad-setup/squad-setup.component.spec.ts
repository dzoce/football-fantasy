import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SquadSetupComponent } from './squad-setup.component';

describe('SquadSetupComponent', () => {
  let component: SquadSetupComponent;
  let fixture: ComponentFixture<SquadSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SquadSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SquadSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
