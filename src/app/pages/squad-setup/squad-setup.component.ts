import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SquadService } from '../../services/squad.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { TeamService } from '../../services/team.service';

const playersToFilter = JSON.parse(localStorage.getItem('ff-all-players'));

@Component({
  selector: 'ff-squad-setup',
  templateUrl: './squad-setup.component.html',
  styleUrls: ['./squad-setup.component.scss']
})
export class SquadSetupComponent implements OnInit {
  balance = 100000;
  hasSquad = localStorage.getItem('ff-hasSquad');
  buttonText = 'Save Team';
  positionsCounter = {
    GK: 0,
    DEF: 0,
    MID: 0,
    ATK: 0
  };

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    if (parseFloat(localStorage.getItem('ff-hasSquad')) === 1) {
        this.router.navigate(['/my-team']);
    }
  }
}
