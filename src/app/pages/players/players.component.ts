import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ff-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
  clubList: any;
  playerList: any = JSON.parse(localStorage.getItem('ff-all-players'));
  settings = {
    columns: {
      name: {
        title: 'Name',
        editable: false,
        valuePrepareFunction: (cell, row) => row.name
      },
      position: {
        title: 'Position',
        editable: false,
        valuePrepareFunction: (cell, row) => row.position
      },
      price: {
        title: 'Price',
        valuePrepareFunction: (cell, row) => row.price
      }
    },
    noDataMessage: 'Players were not found',
    actions: {
      delete: false,
      add: false,
      edit: false
    }
  };
  constructor() { }

  ngOnInit() {

  }

}
