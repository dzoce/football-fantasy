import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Ng4TwitterTimelineService } from 'ng4-twitter-timeline/lib/index';

@Component({
  selector: 'ff-dashboard-news',
  templateUrl: './dashboard-news.component.html',
  styleUrls: ['./dashboard-news.component.scss']
})
export class DashboardNewsComponent implements OnInit {
  settings = {
    sourceType: 'profile',
    screenName: 'breddefantasy'
  };
  options = {tweetLimit: 20};
  leagueID = localStorage.getItem('ff-league');
  latestNews: any;
  constructor(
    private ng4TwitterTimelineService: Ng4TwitterTimelineService,
    private userService: UserService
  ) {}

  ngOnInit() {
      this.getLatestNews();
  }

  getLatestNews() {
    this.userService.getLatestNews({l_id: this.leagueID})
    .subscribe(res => this.latestNews = res.data);
  }
}
