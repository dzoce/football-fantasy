import { UserService } from './../../services/user.service';
import { Component, TemplateRef, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from 'lodash';

@Component({
  selector: 'ff-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @Output() roundChanged: EventEmitter<any> = new EventEmitter();

  modalRef: BsModalRef;
  rounds: any;
  currentRound: number;
  selectedRound = {
    index: 1,
    data: {}
  };
  leagueID = parseFloat(localStorage.getItem('ff-league'));
  private showClass = false;

  constructor(
    private modalService: BsModalService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getCurrentRound({l_id: this.leagueID}).subscribe(result => {
      this.currentRound = parseFloat(result.data);
      console.log(result);
    });

    this.userService.getMatchesByRounds({l_id: this.leagueID}).subscribe(result => {
      result.data.forEach(round => {
        round.round_no = parseFloat(round.round_no);
      });
      setTimeout(() => {
        this.rounds = _.orderBy(result.data, ['round_no']);
        this.selectedRound.data = this.rounds[0];
      }, 0);
    });
  }

   toggleTips(event) {
    // this.showClass = !this.showClass;
    const tips = document.querySelector('.tips_card');
    if (tips.classList.contains('close')) {
      tips.classList.remove('close');
    } else {
      tips.classList.add('close');
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
      this.modalRef.hide();
  }
}
