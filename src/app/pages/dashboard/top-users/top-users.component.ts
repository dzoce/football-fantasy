import { Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'ff-top-users',
    templateUrl: './top-users.component.html',
    styleUrls: ['./top-users.component.scss']
})

export class TopUsersComponent implements OnInit {
    selectedManager: any;
    topManagerList: any;
    modalRef: BsModalRef;
    attackers: any;
    defenders: any;
    midfielders: any;
    goalies: any;
    playerModal: any;
    leagueID = localStorage.getItem('ff-league');
    constructor(
        private router: Router,
        private modalService: BsModalService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.getUsers();
        this.getDreamTeam();
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    closeModal() {
        this.modalRef.hide();
    }

    getUsers() {
        this.userService.getUsers({ l_id: this.leagueID })
            .subscribe(res => {
                this.topManagerList = res.data;
                this.topManagerList.forEach(manager => {
                    manager['startDate'] = moment(manager.created_at).format('DD-MM-YYYY');
                });
            });
    }
    getDreamTeam(){
        this.userService.getDreamTeam({ l_id: this.leagueID })
        .subscribe(res => {
            this.attackers = _.orderBy(res.data.atk, ['total'], ['desc']);
            this.defenders = _.orderBy(res.data.def, ['total'], ['desc']);
            this.midfielders = _.orderBy(res.data.mid, ['total'], ['desc']);
            this.goalies = _.orderBy(res.data.gk, ['total'], ['desc']);
            console.log(res.data);
        });
    }

    viewTeamForManager() {
        this.modalRef.hide();
        this.router.navigate([`/view-team/${this.selectedManager.uuid}`]);
    }

    managerDetails(manager, modalTemplate: TemplateRef<any>) {
        this.modalRef = this.modalService.show(modalTemplate);
        this.selectedManager = manager;
    }
    sendData(template: TemplateRef<any>, player) {
        this.modalRef = this.modalService.show(template);
        this.playerModal = player;
    }

}
