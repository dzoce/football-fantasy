import { Component, OnInit, TemplateRef, ViewEncapsulation, Input, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'ff-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.scss']
})
export class RoundComponent implements OnInit {

  @Input('round') round;
  @Input('isUpcoming') isUpcoming;

  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
  ) {}

  ngOnInit() {
    this.roundFormation();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
      this.modalRef.hide();
  }

  roundFormation() {
    setTimeout(() => {
      this.round.matches.forEach(match => {
        match['formattedTime'] = moment(match.time).format('dddd, D MMMM');
      });
      const groupedMatches = _.groupBy(_.orderBy(this.round.matches, ['time'], ['asc']), 'formattedTime');
      this.round['days'] = _.keys(groupedMatches);
      this.round.matches = _.values(groupedMatches);
    }, 1000);
  }
}
