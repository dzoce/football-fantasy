import { ToastrService } from 'ngx-toastr';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'ff-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  leagueID = localStorage.getItem('ff-league');
  page = 1;
  newsList: any;

  constructor(
    private toastr: ToastrService,
    private userService: UserService
  ) { }

  ngOnInit() {
   this.getNews();
  }

  getNews() {
    this.userService.getNews(this.page, {l_id: this.leagueID}).subscribe(res => {
      this.newsList = res.data;
      console.log(this.newsList);
    });
  }

  nextPage(event) {
    this.page = event;
    this.getNews();
  }

  previousPage(event) {
      this.page = event;
      this.getNews();
  }

  setPage(event) {
    this.page = event;
    this.getNews();
  }
}
