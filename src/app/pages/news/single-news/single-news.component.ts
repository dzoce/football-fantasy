import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'ff-single-news',
  templateUrl: './single-news.component.html',
  styleUrls: ['./single-news.component.scss']
})
export class SingleNewsComponent implements OnInit {
  slug: string;
  article: any;
  showArticle = false;
  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.route.params.subscribe(params => {
      this.slug = params.slug;
    });
  }

  ngOnInit() {
    this.userService.getSingleArticle(this.slug).subscribe(result => {
      this.article = result.data;
      this.showArticle = true;
    });
  }

}
