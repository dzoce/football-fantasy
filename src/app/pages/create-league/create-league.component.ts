import { UserService } from './../../services/user.service';
import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TeamService } from '../../services/team.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
@Component({
  selector: 'ff-create-league',
  templateUrl: './create-league.component.html',
  styleUrls: ['./create-league.component.scss']
})
export class CreateLeagueComponent implements OnInit {
  privateLeagues: any;
  modalRef: BsModalRef;
  leagueID = localStorage.getItem('ff-league');
  currentLeagueModal: any;
  listOfPlayers: any;
  playerPoints: any;
  playerToBan: any;
  creLeagueName: any;
  leagueCode: any;
  inviteMail: any;
  constructor(
    private modalService: BsModalService,
    private teamService: TeamService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
   this.getPrivateLeagues();
  }
  getPrivateLeagues() {
    this.teamService.getPrivateLeagues({l_id: this.leagueID})
    .subscribe(result => {
      this.privateLeagues = result.data;
    });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  sendData(league) {
    this.currentLeagueModal = league;
    console.log(this.currentLeagueModal);
  }

  closeModal() {
      this.modalRef.hide();
  }
  // get players when clicking on league table
  getLeaguePlayers(name) {
    this.teamService.getLeaguePlayers({name: name})
    .subscribe(res => {
      this.listOfPlayers = res.data.users;
      this.playerPoints = _.toArray(res.data.points);
      console.log(this.playerPoints);
    });
  }
  // ban player
  banPlayerId(player) {
    this.playerToBan = player;
  }
  banPlayer(email) {
    this.teamService.banUserFromPrivateLeague({email: email, id: this.currentLeagueModal.id})
    .subscribe(res => {
      this.toastr.success(this.playerToBan.username + ' succesfully banned');
    });
  }
  // delete league
  deleteLeague() {
    this.teamService.deletePrivateLeague({id: this.currentLeagueModal.id})
    .subscribe(res => {
      this.toastr.success('Private league ' + this.currentLeagueModal.name + ' deleted');
      this.getPrivateLeagues();
    });
  }
  // leave league

  leavePrivateLeague() {
    this.teamService.leavePrivateLeague({id: this.currentLeagueModal.id})
    .subscribe(res => {
      this.toastr.success('You have left ' + this.currentLeagueModal.name +  'league');
      this.getPrivateLeagues();
    });
  }
  createLeague() {
    this.teamService.createLeague({name: this.creLeagueName, l_id: this.leagueID})
    .subscribe(res => {
      this.toastr.success('League ' + this.creLeagueName + ' created succesfully', 'Success');
      this.getPrivateLeagues();
    });
  }
  joinLeague() {
    this.teamService.joinPrivateLeague({code: this.leagueCode})
    .subscribe(res => {
      this.toastr.success('Succesfully joined this private league');
      this.getPrivateLeagues();
    });
  }
}
