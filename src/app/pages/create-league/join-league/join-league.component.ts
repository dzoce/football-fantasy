import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../../services/team.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'ff-join-league',
  templateUrl: './join-league.component.html',
  styleUrls: ['./join-league.component.scss']
})
export class JoinLeagueComponent implements OnInit {
  leagueCode: any;
  constructor(
    private teamService: TeamService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }
  joinLeague() {
    this.teamService.joinPrivateLeague({code: this.leagueCode})
    .subscribe(res => {
      this.toastr.success('Private league added');
      this.router.navigate(['leagues/create-league']);
    });
  }
}
