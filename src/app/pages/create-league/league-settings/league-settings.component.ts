import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'ff-league-settings',
  templateUrl: './league-settings.component.html',
  styleUrls: ['./league-settings.component.scss']
})
export class LeagueSettingsComponent implements OnInit {

  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
  ) {


  }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
      this.modalRef.hide();
  }

}
