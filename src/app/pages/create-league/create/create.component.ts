import { ToastrService } from 'ngx-toastr';
import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ff-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
league = {
  name: '',
  l_id: 1
};
  constructor(
    private userService: UserService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }
  // createLeague() {
  //   this.userService.createLeague({name: this.league.name, l_id: this.league.l_id})
  //   .subscribe(res => {
  //     this.toastr.success('League created succesfully', 'Success');
  //     this.router.navigate(['leagues/create-league']);
  //   });
  // }
}
