import { AdminGuard } from './services/admin-guard.service';
import { SquadService } from './services/squad.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';
import { DndListModule } from 'ngx-drag-and-drop-lists';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AccordionModule} from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { routes } from './app.routes';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CountDown } from 'ng4-date-countdown-timer';

import { AppComponent } from './app.component';
import { TopbarComponent } from './shared/components/topbar/topbar.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DashboardNewsComponent } from './pages/dashboard/dashboard-news/dashboard-news.component';
import { BasicLayoutComponent } from './shared/layouts/basic-layout/basic-layout.component';
import { FullPageLayoutComponent } from './shared/layouts/full-page-layout/full-page-layout.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { TeamComponent } from './pages/team/team.component';
import { NewsComponent } from './pages/news/news.component';
import { RoundComponent } from './pages/dashboard/round/round.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { PlayersComponent } from './pages/players/players.component';
import { RankingComponent } from './pages/ranking/ranking.component';
import { HomeComponent } from './pages/home/home.component';
import { FooterComponent } from './pages/home/footer/footer.component';
import { SliderComponent } from './pages/home/slider/slider.component';
import { MyaccountComponent } from './pages/myaccount/myaccount.component';
import { CreateLeagueComponent } from './pages/create-league/create-league.component';
import { ScoreComponent } from './pages/score/score.component';

import { AdminApiService } from './services/admin-api.service';
import { UserService } from './services/user.service';
import { TeamService } from './services/team.service';

import { LeaguesComponent } from './pages/leagues/leagues.component';
import { PricesComponent } from './pages/prices/prices.component';
import { TopUsersComponent } from './pages/dashboard/top-users/top-users.component';
import { Ng4TwitterTimelineModule } from 'ng4-twitter-timeline/lib/index';
import { MainComponent } from './pages/leagues/main/main.component';
import { AdminComponent } from './admin/admin.component';
import { AdminUserComponent } from './admin/admin-user/admin-user.component';
import { AdminSquadComponent } from './admin/admin-squad/admin-squad.component';
import { AdminSeasonComponent } from './admin/admin-season/admin-season.component';
import { AdminRoundComponent } from './admin/admin-round/admin-round.component';
import { AdminPlayerComponent } from './admin/admin-player/admin-player.component';
import { AdminNewsComponent } from './admin/admin-news/admin-news.component';
import { AdminMatchComponent } from './admin/admin-match/admin-match.component';
import { AdminLeagueComponent } from './admin/admin-league/admin-league.component';
import { AdminClubComponent } from './admin/admin-club/admin-club.component';
import { AdminMainComponent } from './admin/admin-main/admin-main.component';
import { JoinLeagueComponent } from './pages/create-league/join-league/join-league.component';
import { CreateComponent } from './pages/create-league/create/create.component';
import { SquadSetupComponent } from './pages/squad-setup/squad-setup.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { TransfersComponent } from './pages/transfers/transfers.component';
import { BuyAndSellComponent } from './shared/components/buy-and-sell/buy-and-sell.component';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SettingsComponent } from './pages/settings/settings.component';
import { SingleNewsComponent } from './pages/news/single-news/single-news.component';
import { PointsComponent } from './pages/points/points.component';
import { ViewManagerTeamComponent } from './pages/view-manager-team/view-manager-team.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AuthGuardService } from './services/auth-guard.service';
import { LeagueSettingsComponent } from './pages/create-league/league-settings/league-settings.component';
import { PaginationComponent } from './shared/components/pagination/pagination.component';
import { SquadGuardService } from './services/squad-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    BasicLayoutComponent,
    FullPageLayoutComponent,
    TopbarComponent,
    SidebarComponent,
    DashboardComponent,
    TeamComponent,
    NewsComponent,
    DashboardNewsComponent,
    RoundComponent,
    NavbarComponent,
    PlayersComponent,
    RankingComponent,
    HomeComponent,
    FooterComponent,
    SliderComponent,
    MyaccountComponent,
    LeaguesComponent,
    PricesComponent,
    TopUsersComponent,
    CreateLeagueComponent,
    MainComponent,
    AdminComponent,
    AdminUserComponent,
    AdminSquadComponent,
    AdminSeasonComponent,
    AdminRoundComponent,
    AdminPlayerComponent,
    AdminNewsComponent,
    AdminMatchComponent,
    AdminLeagueComponent,
    AdminClubComponent,
    AdminMainComponent,
    JoinLeagueComponent,
    CreateComponent,
    SquadSetupComponent,
    ScoreComponent,
    AdminLoginComponent,
    TransfersComponent,
    BuyAndSellComponent,
    SettingsComponent,
    SingleNewsComponent,
    PointsComponent,
    ViewManagerTeamComponent,
    LeagueSettingsComponent,
    PaginationComponent,
    CountDown
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    Ng4TwitterTimelineModule,
    RouterModule.forRoot(
      routes,
      {onSameUrlNavigation: 'reload'}
    ),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    DragulaModule,
    DndListModule,
    ToastrModule.forRoot({
      timeOut: 1500,
      preventDuplicates: true}),
    BsDatepickerModule.forRoot(),
    AccordionModule.forRoot(),
    CollapseModule.forRoot(),
    NgProgressModule.forRoot({
      color: '#8bc34a',
      thick: true
    }),
    NgProgressRouterModule,
    NgProgressHttpModule,
    Ng2SmartTableModule,
    TimepickerModule.forRoot(),
    ImageCropperModule
  ],
  providers: [UserService, TeamService, SquadService, AdminApiService, AdminGuard, AuthGuardService, SquadGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
