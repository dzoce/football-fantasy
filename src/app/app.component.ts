import { TopbarComponent } from './shared/components/topbar/topbar.component';
import { Component } from '@angular/core';


@Component({
  selector: 'ff-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ff';
}
