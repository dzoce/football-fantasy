import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';
import { Component, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from 'lodash';
import { PlayerStats } from './../../models/PlayerStats';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'ff-admin-squad',
  templateUrl: './admin-squad.component.html',
  styleUrls: ['./admin-squad.component.scss']
})
export class AdminSquadComponent implements OnInit {
model = {
  r_id: 0,
  match: 0
};
settings = {
  columns: {
    id: {
      title: 'ID',
      editable: false
    },
    first_name: {
      title: 'Fist Name',
      editable: false
    },
    last_name: {
      title: 'Last Name',
      editable: false
    },
    position: {
      title: 'Position',
      editable: false
    },
    start: {
      title: 'Started Match',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.start === null || row.start === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    k_save: {
      title: 'Keep Save',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.k_save === null || row.k_save === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    score: {
      title: 'Scores'
    },
    assist: {
      title: 'Assists'
    },
    clean: {
      title: 'Clean Sheet',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.clean === null || row.clean === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    miss: {
      title: 'Miss Pen'
    },
    kd_3strike: {
      title: 'Every 3 g'
    },
    yellow: {
      title: 'Yellow Card',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.yellow === null || row.yellow === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    red: {
      title: 'Red Card',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.red === null || row.red === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    own_goal: {
      title: 'Autogoal'
    },
    captain: {
      title: 'Captain',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.captain === null || row.captain === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    sub: {
      title: 'Sub',
      editor: {
        type: 'checkbox',
        config: {
          true: 'Yes',
          false: 'No',
        }
      },
      valuePrepareFunction: (cell, row) => {
       if (row.sub === null || row.sub === '0' ) {
        return 'No';
       }
       return 'Yes';
      },
      class: 'smart_select',
    },
    total: {
      title: 'Total',
      editable: false
    }
  },
  noDataMessage: 'Players for this club were not found',
  actions: {
    delete: false,
    add: false,
    position: 'right'
  },
  edit: {
    confirmSave: true
  }
};

leagueId: any;
matchList: any;
roundList: any;
club1_players: any;
club2_players: any;
showTable =  false;
modalRef: BsModalRef;
playerStatistic: any = [];

constructor(
  private adminApiService: AdminApiService,
  private toastr: ToastrService,
  private modalService: BsModalService
) { }

ngOnInit() {
    this.leagueId = JSON.parse(localStorage.getItem('ff-admin-league'));
    this.getMatchesByRounds();
  }

pushData() {
  this.adminApiService.getClubsByMatch({l_id: this.leagueId, r_id: this.model.r_id, m_id: this.model.match})
  .subscribe((res) => {
    this.club1_players = res.data.club1;
    this.club2_players = res.data.club2;
    this.showTable = true;
  });
}

getMatchesByRounds() {
  this.adminApiService.getMatchesByRounds({l_id: this.leagueId})
  .subscribe((res) => {
    this.roundList = _.orderBy(res.data, ['id']);
    return this.roundList || [];
  });
}
getMatches() {
  this.matchList = this.roundList[this.model.r_id - 1].matches;
}
openModal(template: TemplateRef<any>) {
  this.modalRef = this.modalService.show(template);
}
onEditConfirm(event): void {
  console.log(event);
   event.confirm.resolve(event.newData);
   console.log(event.newData);
  const newObj = _.clone(event.newData, true);
  newObj.assist = parseInt(newObj.assist);
  newObj.score = parseInt(newObj.score);
  newObj.own_goal = parseInt(newObj.own_goal);
  newObj.miss = parseInt(newObj.miss);
  newObj.kd_3strike = parseInt(newObj.kd_3strike);
  if (isNaN(newObj.assist)) {
    newObj.assist = 0;
  }
  if (isNaN(newObj.kd_3strike)) {
    newObj.kd_3strike = 0;
  }
  if (isNaN(newObj.miss)) {
    newObj.miss = 0;
  }
  if (isNaN(newObj.own_goal)) {
    newObj.own_goal = 0;
  }
  if (isNaN(newObj.score)) {
    newObj.score = 0;
  }


  if (newObj.captain === null || newObj.captain === 'No' || newObj.captain  === 0 || newObj.captain === '0' ) {
    newObj.captain = 0;
  } else {
    newObj.captain = 1;
  }
  if (newObj.clean === null || newObj.clean === 'No' || newObj.clean  === 0 || newObj.clean === '0') {
    newObj.clean = 0;
  } else {
    newObj.clean = 1;
  }
  if (newObj.k_save === null || newObj.k_save === 'No' || newObj.k_save  === 0 || newObj.k_save === '0') {
    newObj.k_save = 0;
  } else {
    newObj.k_save = 1;
  }
  if (newObj.red === null || newObj.red === 'No' || newObj.red  === 0 || newObj.red === '0') {
    newObj.red = 0;
  } else {
    newObj.red = 1;
  }
  if (newObj.start === null || newObj.start === 'No' || newObj.start  === 0 || newObj.start === '0') {
    newObj.start = 0;
  } else {
    newObj.start = 1;
  }
  if (newObj.sub === null || newObj.sub === 'No' || newObj.sub  === 0  || newObj.sub === '0') {
    newObj.sub = 0;
  } else {
    newObj.sub = 1;
  }
  if (newObj.yellow === null || newObj.yellow === 'No' || newObj.yellow  === 0  || newObj.yellow === '0') {
    newObj.yellow = 0;
  } else {
    newObj.yellow = 1;
  }

  this.adminApiService.postPlayerStats({data: newObj, l_id: this.leagueId})
  .subscribe((res) => {
    this.pushData();
    this.toastr.success('Sent!');
  });
  console.log(newObj);

  }
}



