import { Component, OnInit } from '@angular/core';
import { AdminApiService } from './../../services/admin-api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ff-admin-player',
  templateUrl: './admin-player.component.html',
  styleUrls: ['./admin-player.component.scss']
})
export class AdminPlayerComponent implements OnInit {
  clubList: any;
  leagueId: any;
  model = {
    firstName: '',
    lastName: '',
    club: '',
    position: '',
    number: 0,
    price: 0
  };
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService
  ) { }


  ngOnInit() {
    this.leagueId = JSON.parse(localStorage.getItem('ff-admin-league'));
    this.showClubs();
  }
  showClubs() {
    this.adminApiService.getClubs({l_id: this.leagueId})
      .subscribe((response) => {
        this.clubList = response.data;
      });
  }

  pushPlayer() {
    const data = {
      first_name: this.model.firstName,
      last_name: this.model.lastName,
      club_name: this.model.club,
      position: this.model.position,
      price: this.model.price,
      number: this.model.number,
      l_id: this.leagueId
    };
    this.adminApiService.postPlayer(data)
    .subscribe((res) => {
      this.toastr.success('Player was added!', 'Success');
    });
  }
}
