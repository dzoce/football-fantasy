import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSeasonComponent } from './admin-season.component';

describe('AdminSeasonComponent', () => {
  let component: AdminSeasonComponent;
  let fixture: ComponentFixture<AdminSeasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSeasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSeasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
