import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';

@Component({
  selector: 'ff-admin-season',
  templateUrl: './admin-season.component.html',
  styleUrls: ['./admin-season.component.scss']
})
export class AdminSeasonComponent implements OnInit {
seasonList: any;
model = {
  seasonName: ''
};
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getSeasons();
  }
  getSeasons() {
    this.adminApiService.getSeasons()
    .subscribe((res) => {
      this.seasonList = res.data;
    });
  }
  removeSeason(id) {
  this.adminApiService.removeSeason({id: id})
  .subscribe(res => {
    this.getSeasons();
    this.toastr.success('Season was removed!', 'Success');
  });
}
  pushSeason() {
  this.adminApiService.addSeason({name: this.model.seasonName})
  .subscribe((res) => {
    this.clearForm();
    this.getSeasons();
    this.toastr.success('Season was added!', 'Success');
  });
}
clearForm() {
  this.model = {
    seasonName: ''
  };
}
}
