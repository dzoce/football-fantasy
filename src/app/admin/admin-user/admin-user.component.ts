import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'ff-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.scss']
})
export class AdminUserComponent implements OnInit {
userList: any;
model = {
  userName: '',
  email: '',
  password: ''
};

edit = {
  id: 0,
  userName: '',
  email: ''
};
remove = {
  id: 0
};
leagueId: any;
modalRef: BsModalRef;
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.leagueId = localStorage.getItem('ff-admin-league');
    this.getUsers();
  }
  getUsers() {
    this.adminApiService.getUsers({l_id: this.leagueId})
    .subscribe((res) => {
      this.userList = res.data;
    });
  }

  pushUser() {
    this.adminApiService.postUser({name: this.model.userName, email: this.model.email, password: this.model.password, l_id: this.leagueId})
    .subscribe((res) => {
      this.clearForm();
      this.getUsers();
      this.toastr.success('User was added!', 'Success');
    });
  }

  removeUser() {
    this.adminApiService.removeUser({uuid: this.remove.id})
    .subscribe(res => {
      this.getUsers();
      this.toastr.success('User was removed!', 'Success');
    });
  }

  clearForm() {
    this.model = {
      userName: '',
      email: '',
      password: ''
    };
  }
  // EDIT USER
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  editUser(){
    this.adminApiService.editUser({id: this.edit.id, name: this.edit.userName, email: this.edit.email})
    .subscribe((res) => {
      this.clearForm();
      this.getUsers();
      this.toastr.success('User was edited!', 'Success');
    });
  }
  sendData(user) {
    this.edit.id = user.uuid;
    this.edit.userName = user.first_name + ' ' + user.last_name;
    this.edit.email = user.email;
  }
    // confirm modal
    deleteModal(id, template) {
      this.remove.id = id;
      this.modalRef = this.modalService.show(template);
    }
}
