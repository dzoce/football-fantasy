import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


@Component({
  selector: 'ff-admin-news',
  templateUrl: './admin-news.component.html',
  styleUrls: ['./admin-news.component.scss']
})
export class AdminNewsComponent implements OnInit {
  leagueId: any;
  newsList: any;
  fileToUpload: string;
  imageChangedEvent: any = '';
    model = {
      title: '',
      body: ''
    };
    edit = {
      title: '',
      body: '',
      image: '',
      id: 0
    };
    remove = {
      id: 0
    };
    modalRef: BsModalRef;
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.showNews();
    this.leagueId = JSON.parse(localStorage.getItem('ff-admin-league'));
  }
  showNews() {
    this.adminApiService.getNews()
    .subscribe((res) => {
      this.newsList = res.data;
    });
  }

  pushNews() {
   const data = {
     title: this.model.title,
      body: this.model.body,
      image: this.fileToUpload,
       l_id: this.leagueId
  };
  this.toastr.info('Please wait while the image is uploading', 'UPLOADING...');
  this.adminApiService.postNews(data)
  .subscribe((res) => {
    this.clearForm();
    this.showNews();
    this.toastr.success('Article is posted', 'Success');
  });
  }

  removeNews() {
    this.adminApiService.removeNews({id: this.remove.id})
    .subscribe(res => {
      this.showNews();
      this.toastr.success('Article is removed!', 'Success');
    });
  }
  clearForm() {
    this.model = {
      title: '',
      body: ''
    };
  }
  // handle photo
  handleFileInput(event: any): void {
      // this.fileToUpload = files.item(0);
      this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
    this.fileToUpload = image;
}
imageLoaded() {
  // show cropper
}
loadImageFailed() {
  // show message
}
    // EDIT Article
    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
    }
    editNews(){
      this.adminApiService.editNews({title: this.edit.title, body: this.edit.body, l_id: this.leagueId, id: this.edit.id})
      .subscribe((res) => {
        this.clearForm();
        this.showNews();
        this.toastr.success('Article is edited', 'Success');
      });
      }
    sendData(news) {
      this.edit.title = news.title;
      this.edit.body = news.body;
      this.edit.id = news.id;
      this.edit.image = news.image_path;
    }
  // confirm modal
  deleteModal(news, template) {
    this.remove.id = news.id;
    this.modalRef = this.modalService.show(template);
  }
}
