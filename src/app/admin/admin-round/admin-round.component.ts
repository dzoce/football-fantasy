import { Component, OnInit, TemplateRef } from '@angular/core';
import { AdminApiService } from './../../services/admin-api.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { timeout } from 'q';
@Component({
  selector: 'ff-admin-round',
  templateUrl: './admin-round.component.html',
  styleUrls: ['./admin-round.component.scss']
})
export class AdminRoundComponent implements OnInit {
  roundList: any;
  leagueId: any;
  disableFlag = false;
  clubList: any;
  numberOfRounds: number;
  time: any = new Date();
  date: any;
  displayDate: any = new Date();
  editDate: any = new Date();
  editTime: any = new Date();
  matchTime: any;
  roundTime: any;
  toggleAll = false;
  model = {
    roundNum: '',
    date: '',
    time: ''
  };
  edit = {
    c1 : '',
    c2 : '',
    r_id : 0,
    m_id: '',
    date: '',
    time: '',
  };
  remove = {
    id: 0
  };
  modalRef: BsModalRef;
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.leagueId = JSON.parse(localStorage.getItem('ff-admin-league'));
    this.getMatchesByRounds();
    this.getRounds();
    this.showClubs();
  }
  toggleAcc() {
    this.toggleAll = !this.toggleAll;
  }
  getRounds() {
    this.adminApiService.getLeague({id: this.leagueId})
    .subscribe((res) => {
    this.numberOfRounds = res.data[0].number_of_rounds;
    });
  }
  getMatchesByRounds() {
    this.adminApiService.getMatchesByRounds({l_id: this.leagueId})
    .subscribe((res) => {
      this.roundList = _.orderBy(res.data, ['id']);
      setTimeout(() => {
          this.disableFlag = false;
      }, 500);
    });
  }

  removeRound() {
    this.adminApiService.removeRound({id: this.remove.id})
    .subscribe((res) => {
      this.getMatchesByRounds();
      this.toastr.success('Round was removed!', 'Success');
    });
  }

  clearForm() {
    this.model = {
      roundNum: '',
      date: '',
      time: ''
    };
  }
  addNewRound() {
    this.disableFlag = true;
    const newRoundId = this.roundList.length + 1;
    if (newRoundId  <= this.numberOfRounds) {
      this.adminApiService.postRound({round_no: newRoundId, l_id: this.leagueId})
      .subscribe((res) => {
        this.getMatchesByRounds();
        this.toastr.success('Round was added!', 'Success');
      });
    } else {
      this.toastr.error('Number of rounds was exceded', 'Error');
    }
  }

  removeMatch() {
    this.adminApiService.removeMatch({id: this.remove.id})
    .subscribe((res) => {
      this.getMatchesByRounds();
      this.toastr.success('Match was removed!', 'Success');
    });
  }
    // DEADLINE SETTINGS

    openDeadline(round, template: TemplateRef<any>, event) {
      event.stopPropagation();
      this.modalRef = this.modalService.show(template);
      this.model.roundNum = round.round_no;
      this.date = moment(round.deadline.split(' ')[0]).toDate();
      console.log(this.date);
      this.time = moment(round.deadline).toDate();
      console.log(this.time);
      this.roundTime = round.deadline;
    }
    setDeadline() {
      console.log(this.model.date, this.model.time);
      const timestamp = this.model.date.concat(' ', this.model.time);
      this.adminApiService.postRoundDeadline({
        l_id: this.leagueId, r_no: this.model.roundNum,
        time: timestamp})
      .subscribe(res => {
        this.toastr.success('Deadline set to ' + timestamp );
        console.log(res);
        this.getMatchesByRounds();
        setTimeout(() => {
          this.roundTime = timestamp;
          console.log(this.roundTime, timestamp);
        }, 50);
      });
    }

    formatTime(event){
      setTimeout(() => {
        this.model.time = moment(this.time).seconds(0).format('HH:mm:ss');
      }, 50);
    }
    formatDate() {
      this.model.date = moment(this.date).format('YYYY-MM-DD');
    }

    // EDIT Match
    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
    }
    editMatch(){
      const date = moment(this.editDate).format('YYYY-MM-DD');
      // const time = moment(this.editTime).format('HH:mm:ss');
      // console.log(date, time);
      console.log(this.editTime);
      const timestamp = date.concat(' ', this.editTime);
    console.log(timestamp);

      const data =  {
        c1_name: this.edit.c1,
        c2_name: this.edit.c2,
        r_no: +this.edit.r_id,
        l_id: this.leagueId,
        id: this.edit.m_id,
        time: timestamp
    };
      this.adminApiService.editMatch(data)
      .subscribe((res) => {
        this.toastr.success('Match was edited!', 'Success');
        this.getMatchesByRounds();
        this.matchTime = timestamp;
      });
    }
    sendData(club1, club2, r_id, m_id, time) {
      if (this.matchTime  ===  undefined) {
        this.matchTime = moment(time).format('YYYY-MM-DD HH:mm:ss');
      }
      this.matchTime = moment(time).format('YYYY-MM-DD HH:mm:ss');
      // this.matchTime = time.toDate();
      this.edit.c1 = club1;
      this.edit.c2 = club2;
      this.edit.r_id = r_id;
      this.edit.m_id = m_id;
      this.editDate = moment(time).toDate();
      this.editTime = moment(time).toDate();
      console.log(time);
      console.log(this.editDate);
      console.log(this.editTime);



    }
    showClubs() {
      this.adminApiService.getClubs({l_id: this.leagueId})
        .subscribe((response) => {
          this.clubList = response.data;
        });
    }
    deleteModal(id, template) {
      this.remove.id = id;
      this.modalRef = this.modalService.show(template);
    }
    formatEditTime(event){
      setTimeout(() => {
        this.editTime = moment(this.editTime).seconds(0).format('HH:mm:ss');
        console.log(this.editTime);
      }, 50);
    }
    formatEditDate() {
      // this.displayDate =  moment(this.displayDate.split(' ')[0]).toDate();
      // this.editDate = moment(this.editDate).format('YYYY-MM-DD');

      console.log(this.editDate);
    }
}
