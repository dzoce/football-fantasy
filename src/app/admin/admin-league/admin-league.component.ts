import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import * as _ from 'lodash';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'ff-admin-league',
  templateUrl: './admin-league.component.html',
  styleUrls: ['./admin-league.component.scss']
})
export class AdminLeagueComponent implements OnInit {
leagueList: any;
sortedLeagueList: any;
model = {
  leagueName: '',
  roundsNum: 0
};
edit = {
  leagueName: '',
  roundsNum: 0,
  id: 0
};
remove = {
  id: 0
};
modalRef: BsModalRef;
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getLeagues();
  }
  getLeagues() {
    this.adminApiService.getLeagues()
    .subscribe((res) => {
      this.leagueList = _.orderBy(res.data, ['id']);
      if (this.leagueList.length === 1) {
        localStorage.setItem('ff-admin-league', this.leagueList[0].id);
      }
    });
  }
removeLeague() {
  this.adminApiService.removeLeague({id: this.remove.id})
  .subscribe(res => {
    this.getLeagues();
    this.toastr.success('League was removed!', 'Success');
  });
}
pushLeague() {
  this.adminApiService.addLeague({name: this.model.leagueName, number_of_rounds: this.model.roundsNum})
  .subscribe((res) => {
    this.clearForm();
    this.getLeagues();
    this.toastr.success('League was added!', 'Success');
    location.reload();
  });
}
clearForm() {
  this.model = {
    leagueName: '',
    roundsNum: null
  };
}
  // EDIT LEAGUE
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  editLeague(){
    this.adminApiService.editLeague({name: this.edit.leagueName, number_of_rounds: this.edit.roundsNum, id: this.edit.id})
    .subscribe((res) => {
      this.toastr.success('League was edited!', 'Success');
    });
  }
  sendData(userName, email, id) {
    this.edit.leagueName = userName;
    this.edit.roundsNum = email;
    this.edit.id = id;
  }

  // confirm modal
  deleteModal(id, template) {
    this.remove.id = id;
    this.modalRef = this.modalService.show(template);
  }
}
