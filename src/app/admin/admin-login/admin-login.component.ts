import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ff-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  email: string;
  password: string;
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }
adminLogin(redirect) {
  this.adminApiService.adminLogin({email: this.email, password: this.password})
  .subscribe(res => {
    if (res.data.role === 'admin') {
      localStorage.setItem('ff-admin-role', 'admin');
      localStorage.setItem('ff-admin-secret', 'jZLNLcdbCe?)z>5DJ,4ZGt9tbR5P:x');
      console.log(res);
      this.toastr.success('Login Successful!', 'Success');
      localStorage.setItem('ff-admin-token', res.data.token);
      localStorage.setItem('ff-admin-user', JSON.stringify(res.data.user));
      this.router.navigate([redirect]);
      this.email = '';
      this.password = '';
    } else {
      this.toastr.error('This is not an admin account', 'Error');
    }
  });
  }
}
