import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminClubComponent } from './admin-club.component';

describe('AdminClubComponent', () => {
  let component: AdminClubComponent;
  let fixture: ComponentFixture<AdminClubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminClubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminClubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
