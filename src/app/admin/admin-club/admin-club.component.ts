import { AdminApiService } from './../../services/admin-api.service';
import { Component, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'ff-admin-club',
  templateUrl: './admin-club.component.html',
  styleUrls: ['./admin-club.component.scss']
})
export class AdminClubComponent implements OnInit {
  clubList: any;
  leagueId: string;
  fileToUpload: string;
  imageChangedEvent: any = '';
  toggleAll = false;
  model = {
    clubName: ''
  };
  edit = {
    clubName: '',
    id: 0
  };
  player = {
    firstName: '',
    lastName: '',
    club: '',
    position: '',
    number: 0,
    price: 0,
    id: 0
  };
  remove = {
    id: 0
  };
  modalRef: BsModalRef;
  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.leagueId = JSON.parse(localStorage.getItem('ff-admin-league'));
    this.showClubsPlayers();
  }
  toggleAcc() {
    this.toggleAll = !this.toggleAll;
  }

  showClubsPlayers() {
    this.adminApiService.getPlayersByClub({l_id: this.leagueId})
    .subscribe((res) => {
      this.clubList = res.data;
      console.log(res.data);
    });
  }
  pushClub() {
    this.adminApiService.postClub({name: this.model.clubName, l_id: this.leagueId, image: this.fileToUpload})
    .subscribe((res) => {
      this.clearForm();
      this.showClubsPlayers();
      this.toastr.success('Club was added!', 'Success');
    });
  }
  clearForm() {
    this.model = {
      clubName: ''
    };
  }
  removeClub() {
    this.adminApiService.removeClub({id: this.remove.id})
    .subscribe(res => {
      this.showClubsPlayers();
      this.toastr.success('Club was removed!', 'Success');
    });
  }
  removePlayer() {
    this.adminApiService.removePlayer({id: this.remove.id})
    .subscribe(res => {
      this.showClubsPlayers();
      this.toastr.success('Player was removed!', 'Success');
    });
    }
    // edit player
  editPlayer() {
  const data = {
        first_name: this.player.firstName,
        last_name: this.player.lastName,
        club_name: this.player.club,
        position: this.player.position,
        price: this.player.price,
        number: this.player.number,
        l_id: this.leagueId,
        id: this.player.id
    };
      this.adminApiService.editPlayer(data)
      .subscribe((res) => {
        this.showClubsPlayers();
        this.toastr.success('Player was added!', 'Success');
      });
    }
    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
    }
    sendData(first, last, club, position, price, number, id ) {
      this.player.firstName = first;
      this.player.lastName = last;
      this.player.club = club;
      this.player.position = position;
      this.player.price = price;
      this.player.number = number;
      this.player.id = id;
    }
  // edit club
  editClub(){
    this.adminApiService.editClub({name: this.edit.clubName, l_id: this.leagueId, id: this.edit.id})
    .subscribe((res) => {
      this.clearForm();
      this.showClubsPlayers();
      this.toastr.success('Club was edited!', 'Success');
    });
  }
  sendClubData(name, id) {
    this.edit.clubName = name;
    this.edit.id = id;
  }
  // Confirm modal

  deleteModal(id, template) {
    this.remove.id = id;
    this.modalRef = this.modalService.show(template);
  }

    // handle photo
    handleFileInput(event: any): void {
      // this.fileToUpload = files.item(0);
      this.imageChangedEvent = event;
  }
  imageCropped(image: string) {
    this.fileToUpload = image;
}
imageLoaded() {
  // show cropper
}
loadImageFailed() {
  // show message
}
}
