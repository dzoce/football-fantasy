import { AdminApiService } from './../../services/admin-api.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Component, OnInit, OnChanges, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'ff-admin-main',
  templateUrl: './admin-main.component.html',
  styleUrls: ['./admin-main.component.scss']
})
export class AdminMainComponent implements OnInit, OnChanges {
admin = JSON.parse(localStorage.getItem('ff-admin-user'));
availableLeagues: any = [];
league: any;
league_id: any;
isCollapsed = true;
modalRef: BsModalRef;
  constructor(
    private router: Router,
    private adminApiService: AdminApiService,
    private modalService: BsModalService
    ) { }

  ngOnInit() {
    this.getLeagues();
  }

  ngOnChanges() {
    this.getLeagues();
  }

  getLeagues() {
    this.adminApiService.getLeagues()
    .subscribe((res) => {
      this.availableLeagues = res.data;
      if (this.availableLeagues.length > 0) {
        if (localStorage.getItem('ff-admin-league')) {
          this.league_id = JSON.parse(localStorage.getItem('ff-admin-league'));
          this.league = _.find(this.availableLeagues, { 'id': this.league_id});
        }
        else {
          this.league = this.availableLeagues[0];
          localStorage.setItem('ff-admin-league', this.availableLeagues[0].id);
        }
      }
    });
  }

  setItem() {
    localStorage.setItem('ff-admin-league', JSON.stringify(this.league.id));
    location.reload();
  }
  adminLogout() {
      localStorage.removeItem('ff-admin-user');
      localStorage.removeItem('ff-admin-token');
      localStorage.removeItem('ff-admin-role');
      localStorage.removeItem('ff-admin-secret');
      localStorage.removeItem('ff-admin-league');
      this.router.navigate(['/admin/login']);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
