import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AdminApiService } from './../../services/admin-api.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'ff-admin-match',
  templateUrl: './admin-match.component.html',
  styleUrls: ['./admin-match.component.scss']
})
export class AdminMatchComponent implements OnInit {

  clubList: any;
  roundList: any;
  leagueId: any;
  date: any;
  time: any = new Date();
  model = {
    c1 : '',
    c2 : '',
    r_id : 0,
    time: '',
    date: ''
  };

  constructor(
    private adminApiService: AdminApiService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.leagueId = JSON.parse(localStorage.getItem('ff-admin-league'));
    this.showClubs();
    this.getRounds();
  }

  pushMatch() {
    const timestamp = this.model.date.concat(' ', this.model.time);
    console.log(timestamp);
    this.adminApiService.postMatch({c1_name: this.model.c1, c2_name: this.model.c2,
    r_no: +this.model.r_id, l_id: this.leagueId, time: timestamp})
    .subscribe((res) => {
      this.toastr.success('Match was added!', 'Success');
    });
  }
  showClubs() {
    this.adminApiService.getClubs({l_id: this.leagueId})
      .subscribe((response) => {
        this.clubList = response.data;
      });
  }

  getRounds() {
    this.adminApiService.getRounds({l_id: this.leagueId})
    .subscribe((res) => {
      this.roundList = _.orderBy(res.data, ['id']);
    });
  }

  formatTime(){
    setTimeout(() => {
      this.model.time = moment(this.time).seconds(0).format('HH:mm:ss');
    }, 50);
  }
  formatDate() {
    this.model.date = moment(this.date).format('YYYY-MM-DD');
  }
}
