import { Player } from './Player';

export class FieldPosition {
    position: string;
    data: Array<Player>;

    constructor(position = '', data = []) {
       this.position = position;
       this.data = data;
    }
}
