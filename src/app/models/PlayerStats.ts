

export interface PlayerStats {
    player_id: number;
    first_name: string;
    last_name: string;
    position: string;
    // start_match: boolean;
    // keeper_scores: boolean;
    // defender_scores: boolean;
    // midfielder_scores: boolean;
    // attacker_scores: boolean;
    // assists: number;
    // gk_def_cs: boolean;
    // mid_cs: boolean;
    // gk_sp: number;
    // pl_mp: number;
    // e3cgk_d: number;
    // yc: boolean;
    // rc: boolean;
    // own_goal: number;
    // captain: boolean;
}
