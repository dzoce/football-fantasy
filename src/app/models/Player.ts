export class Player {
    number: number;
    position: string;
    club: any = {name: ''};
    price: number;

    constructor(number = 0, position = '', club = { name: '' }, price = 0) {
        this.number = number;
        this.position = position;
        this.club = club;
        this.price = price;
    }
}
