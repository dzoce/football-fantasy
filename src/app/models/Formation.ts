export class Formation {
    name: string;
    positions: Array<string>;
}
