import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'ff-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input('items') items;
  @Input('type') type;
  @Output() pageNext: EventEmitter<any> = new EventEmitter();
  @Output() pagePrevious: EventEmitter<any> = new EventEmitter();
  @Output() pageSet: EventEmitter<any> = new EventEmitter();

  page: number;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.page = this.userService.page;
  }

  nextPage() {
    if (this.items.next_page_url !== null) {
      this.page += 1;
      this.pageNext.emit(this.page);
    }
  }

  previousPage() {
    if (this.items.prev_page_url !== null) {
      this.page -= 1;
      this.pagePrevious.emit(this.page);
    }
  }

  setPage(page) {
    this.page = page;
    this.pageSet.emit(this.page);
  }
}
