import { Router, NavigationEnd } from '@angular/router';
import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { UserService } from '../../../services/user.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ff-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  isMenuOpen = false;
  messages: any = [];
  hasSquad = parseFloat(localStorage.getItem('ff-hasSquad')) === 1 ? true : false;
  availableLeagues = JSON.parse(localStorage.getItem('ff-myLeagues'));
  selectedLeague: any;

  @ViewChild('menu') menu: ElementRef;
  @ViewChild('menuTrigger') menuTrigger: ElementRef;

  @HostListener('document:click', ['$event'])
  onDocumentClick(e) {
    if (!(this.menuTrigger.nativeElement).contains(e.target)) {
      this.isMenuOpen = false;
    }
  }

  constructor(
    private router: Router,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          if (localStorage.getItem('ff-user')) {
            this.userService.getMyLeagues().subscribe(result => {
              this.availableLeagues = result.data;
              setTimeout(() => {
                if (!localStorage.getItem('ff-league')) {
                  this.selectedLeague = this.availableLeagues[0];
                  localStorage.setItem('ff-league', this.selectedLeague.id);
                }
                else {
                  this.selectedLeague = _.find(this.availableLeagues, ['id', parseFloat(localStorage.getItem('ff-league'))]);
                }
              }, 50);
            });
          }
        }
    });
  }

  get user(): any {
    return JSON.parse(localStorage.getItem('ff-user'));
  }

  switchLeague(league) {
    localStorage.setItem('ff-league', league.id);
    window.location.reload();
  }

  logout() {
    localStorage.removeItem('ff-user');
    localStorage.removeItem('ff-token');
    localStorage.removeItem('ff-myLeagues');
    localStorage.removeItem('ff-league');
    localStorage.removeItem('ff-userRole');
    localStorage.removeItem('ff-hasSquad');
    localStorage.removeItem('ff-all-players');
    localStorage.removeItem('ff-all-clubs');
    this.router.navigate(['/home']);
    this.toastr.info('You have been logged out.', 'Info');
  }

  toggleNav() {
    this.isMenuOpen = !this.isMenuOpen;
  }
}
