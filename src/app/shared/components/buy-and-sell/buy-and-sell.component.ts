import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SquadService } from '../../../services/squad.service';
import { TeamService } from '../../../services/team.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';

let playersToFilter: any;

@Component({
    selector: 'ff-buy-and-sell',
    templateUrl: './buy-and-sell.component.html',
    styleUrls: ['./buy-and-sell.component.scss']
})
export class BuyAndSellComponent implements OnInit {
    @Input('balance') balance;
    @Input('buttonText') buttonText;
    @Input('buttonFunction') buttonFunction;
    @Input('selectedPlayers') selectedPlayers;
    @Input('points') points;

    @Output() makeTransfer = new EventEmitter<any>();

    clubs: Array<any> = [];
    availablePlayers: Array<any> = [];
    startTeam = [];
    substitutions = [];
    leagueID: any;
    positionsCounter = {
        GK: 0,
        DEF: 0,
        MID: 0,
        ATK: 0
    };

    playersByClubCounter = {};

    filters = {
        view: '',
        sortBy: 'total_points',
        maxPrice: '',
        name: '',
        orderBy: {
            priceDesc: false
        }
    };

    transfersLeft = 0;
    transferLeftInDb = 0;
    transfers = {
        buy: [],
        sell: [],
        money: this.balance
    };

    constructor(
        private router: Router,
        private squadService: SquadService,
        private teamService: TeamService,
        private toastr: ToastrService
    ) {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.leagueID = localStorage.getItem('ff-league');
                this.initializeData();
            }
        });
    }

    ngOnInit() {
        if (this.buttonFunction === 'update') {
            this.teamService.checkTransfers({ l_id: this.leagueID }).subscribe(result => {
                this.transferLeftInDb = parseFloat(result.data);
                this.transfersLeft = this.transferLeftInDb;
            });
        }
        else {
            this.leagueID = localStorage.getItem('ff-league');
            this.initializeData();
        }
    }

    buyPlayer(player) {
        switch (player.position) {
            case 'GK':
                this.executeBuyPlayer(player, 'GK', 2);
                break;
            case 'DEF':
                this.executeBuyPlayer(player, 'DEF', 5);
                break;
            case 'MID':
                this.executeBuyPlayer(player, 'MID', 5);
                break;
            case 'ATK':
                this.executeBuyPlayer(player, 'ATK', 3);
                break;
        }
    }

    removePlayer(player, index) {
        switch (player.position) {
            case 'GK':
                this.executeRemovePlayer(player);
                break;
            case 'DEF':
                this.executeRemovePlayer(player);
                break;
            case 'MID':
                this.executeRemovePlayer(player);
                break;
            case 'ATK':
                this.executeRemovePlayer(player);
                break;
        }
    }

    updateTeam() {
        this.transfers.money = this.balance;
        this.makeTransfer.emit(this.transfers);
    }

    saveTeam() {
        const goalkeeper = _.filter(this.selectedPlayers, { position: 'GK' });
        const defenders = _.filter(this.selectedPlayers, { position: 'DEF' });
        const midfielders = _.filter(this.selectedPlayers, { position: 'MID' });
        const forwards = _.filter(this.selectedPlayers, { position: 'ATK' });

        this.startTeam = _.concat(
            _.take(goalkeeper, 1),
            _.take(defenders, 4),
            _.take(midfielders, 4),
            _.take(forwards, 2)
        );

        this.substitutions = _.concat(
            _.drop(goalkeeper, 1),
            _.drop(defenders, 4),
            _.drop(midfielders, 4),
            _.drop(forwards, 2)
        );

        const finalSquadObject = {
            'l_id': this.leagueID,
            'money': this.balance,
            'selected_team': _.map(this.startTeam, 'id'),
            'substitutions': _.map(this.substitutions, 'id')
        };

        if ((_.concat(this.startTeam, this.substitutions).length) === 15) {
            this.squadService.postSquad(finalSquadObject).subscribe(response => {
                if (response.data) {
                    this.toastr.success('Your team is created successfully!', 'Success');
                    localStorage.setItem('ff-hasSquad', '1');
                    setTimeout(() => {
                        window.location.reload();
                    }, 500);
                }
            });
        }
        else {
            this.toastr.error('You must buy 15 players for your team!', 'Error');
        }
    }

    removeTransfer(index) {
        if (this.transfers.buy[index]) {
            this.balance += (this.transfers.buy[index].price - this.transfers.sell[index].price);
            this.selectedPlayers.unshift(this.transfers.sell[index]);
            this.selectedPlayers.unshift(this.transfers.buy[index]);
            this.transfers.sell.splice(index, 1);
            this.transfers.buy.splice(index, 1);
        }
        else {
            this.balance -= this.transfers.sell[index].price;
            this.selectedPlayers.unshift(this.transfers.sell[index]);
            this.positionsCounter[this.transfers.sell[index].position] += 1;
            this.playersByClubCounter[this.transfers.sell[index].club.name] += 1;
            this.transfers.sell.splice(index, 1);
        }
        if (this.transfersLeft < this.transferLeftInDb) {
            this.transfersLeft += 1;
        }
    }

    filterPlayers() {
        if (this.filters.view === '' && this.filters.maxPrice === '' && this.filters.name === '') {
            this.availablePlayers = playersToFilter;
            console.log(this.availablePlayers);
            
        }
        else {
            let filteredData = playersToFilter;
            if (this.filters.view !== '') {
                if (['GK', 'DEF', 'MID', 'ATK'].indexOf(this.filters.view) >= 0) {
                    filteredData = _.filter(filteredData, { position: this.filters.view });
                }
                else {
                    filteredData = _.filter(filteredData, (player) => {
                        return player.club.name === this.filters.view;
                    });
                }
            }
            if (this.filters.maxPrice !== '') {
                filteredData = _.filter(filteredData, (player) => {
                    return parseFloat(player.price) <= parseFloat(this.filters.maxPrice);
                });
            }
            if (this.filters.name !== '') {
                filteredData = _.filter(filteredData, (player) => {
                    return (player.name).includes(this.filters.name, 0);
                });
            }
            this.availablePlayers = filteredData;
            console.log(this.availablePlayers);
        }
    }

    orderByPrice() {
        this.filters.orderBy.priceDesc = !this.filters.orderBy.priceDesc;
        this.availablePlayers = _.orderBy(this.availablePlayers, ['price'], [this.filters.orderBy.priceDesc ? 'desc' : 'asc']);
    }

    private executeBuyPlayer(player, position, length) {
        const alreadySelected = _.filter(this.selectedPlayers, { 'position': position });
        if (alreadySelected.length < length) {
            if (this.playersByClubCounter[player.club.name]) {
                if (this.playersByClubCounter[player.club.name] < 3) {
                    this.playersByClubCounter[player.club.name] += 1;

                    if (this.balance - player.price > 0) {
                        this.balance -= player.price;
                        const selectedPlayer = _.findIndex(this.availablePlayers, { 'id': player.id });
                        this.selectedPlayers.unshift(player);
                        this.availablePlayers.splice(selectedPlayer, 1);
                        this.positionsCounter[position] += 1;

                        if (this.transfersLeft > 0) {
                            this.transfers.buy.push(player);
                            this.transfersLeft -= 1;
                        }
                        else {
                            this.transfers.buy.push(player);
                            this.points = this.points === 0 ? 4 : this.points * 2;
                        }
                    }
                    else {
                        this.toastr.error(`You don't have enough money to buy this player`, 'Error');
                    }
                }
                else {
                    this.toastr.error('You can select maximum 3 players from same club!', 'Error');
                }
            }
            else {
                this.playersByClubCounter[player.club.name] = 1;
                if (this.balance - player.price > 0) {
                    this.balance -= player.price;
                    const selectedPlayer = _.findIndex(this.availablePlayers, { 'id': player.id });
                    this.selectedPlayers.unshift(player);
                    this.availablePlayers.splice(selectedPlayer, 1);
                    this.positionsCounter[position] += 1;
                    this.transfers.buy.push(player);
                    if (this.transfersLeft > 0) {
                        this.transfersLeft -= 1;
                    }
                    else {
                        this.points = this.points === 0 ? 4 : this.points * 2;
                    }
                }
                else {
                    this.toastr.error(`You don't have enough money to buy this player`, 'Error');
                }
            }
        }
        else {
            this.toastr.error(`You have maximum number of players for ${position} position!`, 'Error');
        }
    }

    private executeRemovePlayer(player) {
        const index = _.findIndex(this.selectedPlayers, { 'first_name': player.first_name });

        if (this.transfers.sell.length === this.transfers.buy.length) {
            this.transfers.sell.push(player);
            this.balance += parseInt(player.price);
            this.selectedPlayers.splice(index, 1);
            this.playersByClubCounter[player.club.name] -= 1;
            this.positionsCounter[player.position] -= 1;
        }
        if (this.buttonFunction === 'save') {
            this.availablePlayers.unshift(player);
            this.balance += parseInt(player.price);
            this.selectedPlayers.splice(index, 1);
            this.playersByClubCounter[player.club.name] -= 1;
            this.positionsCounter[player.position] -= 1;
        }


        if (this.playersByClubCounter[player.club.name] === 0) {
            delete this.playersByClubCounter[player.club.name];
        }
    }

    initializeData() {
        this.leagueID = localStorage.getItem('ff-league');
        this.teamService.getClubs({ l_id: this.leagueID }).subscribe(res => {
            localStorage.setItem('ff-all-clubs', JSON.stringify(res.data));
            this.clubs = res.data;
        });
        this.teamService.getPlayers().subscribe(result => {
            result.data.forEach(player => {
                player.name = `${player.first_name} ${player.last_name}`;
                player.price = parseFloat(player.price);
            });

            if (this.selectedPlayers !== undefined) {
                setTimeout(() => {
                    this.selectedPlayers.forEach(player => {
                        this.positionsCounter[player.position] += 1;
                        if (!this.playersByClubCounter[player.club.name]) {
                            this.playersByClubCounter[player.club.name] = 0;
                        }
                        this.playersByClubCounter[player.club.name] += 1;
                        _.remove(result.data, (item) => {
                            return item.id === player.id;
                        });
                    });
                }, 500);
            }
            else {
                this.selectedPlayers = [];
            }

            setTimeout(() => {
                if (localStorage.getItem('ff-all-players')) {
                    localStorage.setItem('ff-all-players', JSON.stringify(result.data));
                    this.availablePlayers = result.data;
                    playersToFilter = result.data;
                }
                else {
                    localStorage.setItem('ff-all-players', JSON.stringify(result.data));
                    this.availablePlayers = result.data;
                    playersToFilter = result.data;
                }
            }, 100);
        });
    }
}
