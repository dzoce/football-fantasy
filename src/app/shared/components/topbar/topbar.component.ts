import { Component, TemplateRef, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SquadService } from '../../../services/squad.service';

@Component({
  selector: 'ff-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  email: string;
  password: string;

  registration = {
    email: '',
    username: '',
    first_name: '',
    last_name: '',
    league: '1',
    password: '',
    passwordConfirm: ''
  };

  availableLeagues = [];

  modalRef: BsModalRef;
  constructor(
    private userService: UserService,
    private router: Router,
    private toastr: ToastrService,
    private squadService: SquadService,
    private modalService: BsModalService
  ) {
      this.email = '';
      this.password = '';
    }

  ngOnInit() {
    this.userService.getAllLeagues().subscribe(result => {
      this.availableLeagues = result.data;
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
    this.modalRef.hide();
  }

  login(redirectTo) {
    this.userService.login({email: this.email, password: this.password}).subscribe(res => {
    console.log(res);
      if (res.data.error) {
        this.toastr.error('Wrong credentials!', 'Error');
      }
      if (res.data.user) {
        this.modalRef.hide();
        this.toastr.success('Login Successful!', 'Success');
        localStorage.setItem('ff-token', res.data.token);
        localStorage.setItem('ff-userRole', res.data.role);
        localStorage.setItem('ff-myLeagues', JSON.stringify(res.data.leagues));
        localStorage.setItem('ff-league', JSON.stringify(res.data.leagues[0]));
        localStorage.setItem('ff-user', JSON.stringify(res.data.user));
        this.getHasSquad(res.data.leagues[0]);

        setTimeout(() => {
          this.email = '';
          this.password = '';
          window.location.reload();
          //this.router.navigate([redirectTo]);
        }, 500);
      }
    });
  }

  register() {
    if (this.registration.password === this.registration.passwordConfirm) {
      this.userService.register({
        email: this.registration.email,
        league: this.registration.league,
        username: this.registration.username,
        first_name: this.registration.first_name,
        last_name: this.registration.last_name,
        password: this.registration.password
      }).subscribe(res => {
        this.modalRef.hide();
        this.email = this.registration.email;
        this.password = this.registration.password;
        this.login('squad-setup');
        this.clearSignupFields();
      });
    } else {
      this.toastr.error('Passwords not match!');
    }
  }

  clearSignupFields() {
    this.registration = {
      email: '',
      username: '',
      league: '1',
      first_name: '',
      last_name: '',
      password: '',
      passwordConfirm: ''
    };
  }

  getHasSquad(leagueID) {
    this.squadService.hasSquad({l_id: leagueID}).subscribe(result => {
      localStorage.setItem('ff-hasSquad', result.data);
    });
  }
}
