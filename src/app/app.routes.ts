import { AuthGuardService } from './services/auth-guard.service';
import { AdminGuard } from './services/admin-guard.service';
import { TransfersComponent } from './pages/transfers/transfers.component';
import { FullPageLayoutComponent } from './shared/layouts/full-page-layout/full-page-layout.component';
import { Route } from '@angular/router';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { BasicLayoutComponent } from './shared/layouts/basic-layout/basic-layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { TeamComponent } from './pages/team/team.component';
import { PlayersComponent } from './pages/players/players.component';
import { RankingComponent } from './pages/ranking/ranking.component';
import {HomeComponent} from './pages/home/home.component';
import {MyaccountComponent} from './pages/myaccount/myaccount.component';
import {LeaguesComponent} from './pages/leagues/leagues.component';
import {PricesComponent} from './pages/prices/prices.component';
import {CreateLeagueComponent} from './pages/create-league/create-league.component';
import {MainComponent} from './pages/leagues/main/main.component';
import {JoinLeagueComponent} from './pages/create-league/join-league/join-league.component';
import {CreateComponent} from './pages/create-league/create/create.component';
import {ScoreComponent} from './pages/score/score.component';
import {SettingsComponent} from './pages/settings/settings.component';
import {NewsComponent} from './pages/news/news.component';
import {PointsComponent} from './pages/points/points.component';
import {LeagueSettingsComponent} from './pages/create-league/league-settings/league-settings.component';
// admin
import { AdminComponent } from './admin/admin.component';
import { AdminUserComponent } from './admin/admin-user/admin-user.component';
import { AdminClubComponent } from './admin/admin-club/admin-club.component';
import { AdminLeagueComponent } from './admin/admin-league/admin-league.component';
import { AdminMatchComponent } from './admin/admin-match/admin-match.component';
import { AdminNewsComponent } from './admin/admin-news/admin-news.component';
import { AdminPlayerComponent } from './admin/admin-player/admin-player.component';
import { AdminRoundComponent } from './admin/admin-round/admin-round.component';
import { AdminSeasonComponent } from './admin/admin-season/admin-season.component';
import { AdminSquadComponent } from './admin/admin-squad/admin-squad.component';
import { AdminMainComponent } from './admin/admin-main/admin-main.component';
import { SquadSetupComponent } from './pages/squad-setup/squad-setup.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { SingleNewsComponent } from './pages/news/single-news/single-news.component';
import { ViewManagerTeamComponent } from './pages/view-manager-team/view-manager-team.component';
import { SquadGuardService } from './services/squad-guard.service';




export const routes: Route[] = [
    { path: 'home', component: HomeComponent, runGuardsAndResolvers: 'always' },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {
        path: '', component: FullPageLayoutComponent,
        children: [
            {
                path: 'admin',
                component: AdminComponent,
                children: [
                    {
                        path: 'login',
                        component: AdminLoginComponent
                    },
                    {
                        path: '', redirectTo: 'login', pathMatch: 'full'
                    },
                    {
                        path: 'main',
                        component: AdminMainComponent,
                        canActivate: [AdminGuard],
                        canActivateChild: [AdminGuard],
                        children: [
                            {    path: '', redirectTo: 'club', pathMatch: 'full'},
                            {
                                path: 'league',
                                component: AdminLeagueComponent
                            },
                            {
                                path: 'user',
                                component: AdminUserComponent
                            },
                            {
                                path: 'squad',
                                component: AdminSquadComponent
                            },
                            {
                                path: 'season',
                                component: AdminSeasonComponent
                            },
                            {
                                path: 'round',
                                component: AdminRoundComponent
                            },
                            {
                                path: 'player',
                                component: AdminPlayerComponent
                            },
                            {
                                path: 'news',
                                component: AdminNewsComponent
                            },
                            {
                                path: 'match',
                                component: AdminMatchComponent
                            },
                            {
                                path: 'club',
                                component: AdminClubComponent
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        // Basic layout routes definition
        path: '', component: BasicLayoutComponent,
        children: [
            {
                path: 'prices',
                component: PricesComponent
            },
            {
                path: 'news',
                children: [
                    {
                        path: '',
                        component: NewsComponent
                    },
                    {
                        path: ':slug',
                        component: SingleNewsComponent
                    }
                ]
            },
            {
                path: 'dashboard',
                component: DashboardComponent,
                canActivate: [AuthGuardService, SquadGuardService],
                runGuardsAndResolvers: 'always'
            },
            {
                path: 'settings',
                component: SettingsComponent,
                canActivate: [AuthGuardService, SquadGuardService]
            },
            {
                path: 'squad-setup',
                component: SquadSetupComponent,
                canActivate: [AuthGuardService],
                runGuardsAndResolvers: 'always'
            },
            {
                path: 'my-team',
                component: TeamComponent,
                canActivate: [AuthGuardService, SquadGuardService],
                runGuardsAndResolvers: 'always'
            },
            {
                path: 'transfers',
                component: TransfersComponent,
                canActivate: [AuthGuardService, SquadGuardService],
                runGuardsAndResolvers: 'always'
            },
            {
                path: 'myaccount',
                component: MyaccountComponent,
                canActivate: [AuthGuardService, SquadGuardService]
            },
            {
                path: 'league-settings',
                component: LeagueSettingsComponent,
                canActivate: [AuthGuardService, SquadGuardService]
            },
            {
                path: 'leagues',
                component: LeaguesComponent,
                canActivate: [AuthGuardService, SquadGuardService],
                children: [
                    {
                        path: '', redirectTo: 'create-league', pathMatch: 'full'
                    },
                    // {
                    //     path: 'main',
                    //     component: MainComponent,
                    // },
                   {
                        path: 'create-league',
                        component: CreateLeagueComponent,
                    },
                    {
                        path: 'create',
                        component: CreateComponent,
                    },
                    {
                        path: 'join-league',
                        component: JoinLeagueComponent,
                    },

                ]
            },
            {
                path: 'players',
                component: PlayersComponent,
                canActivate: [AuthGuardService, SquadGuardService],
            },
            {
                path: 'ranking',
                component: RankingComponent,
                canActivate: [AuthGuardService, SquadGuardService],
            },
            {
                path: 'score',
                component: ScoreComponent,
                canActivate: [AuthGuardService, SquadGuardService],
            },
            {
                path: 'points',
                component: PointsComponent,
                canActivate: [AuthGuardService, SquadGuardService],
            },
            {
                path: 'view-team/:managerUuid',
                component: ViewManagerTeamComponent,
                canActivate: [AuthGuardService, SquadGuardService],
            },
        ]
    },
    { path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
];
