import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { SquadService } from './squad.service';

@Injectable()
export class SquadGuardService implements CanActivate {
  hasSquad = parseFloat(localStorage.getItem('ff-hasSquad'));

  constructor(
    private squadService: SquadService,
    public router: Router
  ) {}


  canActivate(): boolean {
    if (this.hasSquad === 0) {
      this.router.navigate(['/squad-setup']);
      return false;
    }
    return true;
  }
}
