
import { Injectable } from '@angular/core';
import { CanActivateChild, RouterStateSnapshot, ActivatedRouteSnapshot, Router, CanActivate} from '@angular/router';
import { Observable } from 'rxjs/Observable';


@Injectable()

export class AdminGuard implements CanActivate, CanActivateChild {
    constructor(private router: Router) {
    }
    isAuthenticated(): boolean{
        if (
            localStorage.getItem('ff-admin-secret') === 'jZLNLcdbCe?)z>5DJ,4ZGt9tbR5P:x' &&
            localStorage.getItem('ff-admin-role') === 'admin'
        ) {
          return true;
        }
        else{
          return false;
        }
      }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if (!this.isAuthenticated()) {
          this.router.navigate(['/admin/login']);
          return false;
        }
        return true;
      }
      canActivate(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if (!this.isAuthenticated()) {
          this.router.navigate(['/admin/login']);
          return false;
        }
        return true;
      }
}
