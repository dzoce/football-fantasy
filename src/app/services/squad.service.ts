import { ToastrService } from 'ngx-toastr';
import { Http, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { API } from '../app.constants';

@Injectable()
export class SquadService {

  constructor(
    private http: Http,
    private toastr: ToastrService
  ) { }

  postSquad(data): Observable<any>{
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'myTeam/postSquad', data, {headers: headers})
    .map(res => res.json());
  }

  updateSquad(data): Observable<any>{
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.put(API + 'myTeam/updateSquad', data, {headers: headers})
    .map(res => res.json());
  }

  getUserSquad(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'getUserSquad', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get user squad!', 'Error');
        return Observable.throw(err);
      });
  }

  hasSquad(data): Observable<any>{
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'myTeam/hasSquad', data, {headers: headers})
    .map(res => res.json());
  }

}
