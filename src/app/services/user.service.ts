import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
import { API } from '../app.constants';

@Injectable()
export class UserService {

  constructor(
    private http: Http,
    private toastr: ToastrService
  ) {}

  page = 1;

  login(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(API + 'user/login', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Wrong Credentials', 'Error');
        return Observable.throw(err);
      });
  }

  register(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(API + 'user/register', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('All fields are mandatory!', 'Error');
        return Observable.throw(err);
      });
  }

  getUserSettings(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.get(API + 'getUserSettings', {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get user settings!', 'Error');
        return Observable.throw(err);
      });
  }

  updateUserSettings(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.put(API + 'updateUserSettings', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('An error occurred while updating your profile!', 'Error');
        return Observable.throw(err);
    });
  }

  getAllLeagues(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(API + 'getAllLeagues', {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get available leagues!', 'Error');
        return Observable.throw(err);
      });
  }

  getMyLeagues(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.get(API + 'getMyLeagues', {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get my leagues!', 'Error');
        return Observable.throw(err);
      });
  }

  getUsers(data, page?): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + `users?page=${page}`, data, {headers: headers})
      .map(response => response.json())
      .catch((err) => {
        this.toastr.error('Bad Request', 'Error');
        return Observable.throw(err);
      });
  }
  getNews(page, data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + `getNews?page=${page}`, data, {headers: headers})
      .map(response => response.json())
      .catch((err) => {
          this.toastr.error('Bad Request', 'Error');
          return Observable.throw(err);
      });
  }
    getLatestNews(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
      return this.http
                  .post(API + 'getLatestNews', data, {headers: headers})
                  .map(response => response.json())
                  .catch((err) => {
                    this.toastr.error('Bad Request', 'Error');
                    return Observable.throw(err);
                });
        }
  getSingleArticle(slug): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.get(API + `getArticle/${slug}`, {headers: headers})
      .map(response => response.json())
      .catch((err) => {
        this.toastr.error('Bad Request', 'Error');
        return Observable.throw(err);
    });
  }
      getDreamTeam(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
        return this.http
                    .post(API + 'getDreamTeam', data, {headers: headers})
                    .map(response => response.json())
                    .catch((err) => {
                      this.toastr.error('Bad Request', 'Error');
                      return Observable.throw(err);
                  });
          }
      getTopFivePlayers() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.get(API + 'topFivePlayers', {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to get top 5 players!', 'Error');
            return Observable.throw(err);
          });
      }
      getTopFivePlayersDivisionOne() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.get(API + 'topFivePlayersDivision1', {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to get top 5 players!', 'Error');
            return Observable.throw(err);
          });
      }
      getTopFivePlayersDivisionTwo() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.get(API + 'topFivePlayersDivision2', {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to get top 5 players!', 'Error');
            return Observable.throw(err);
          });
      }

  getCurrentRound(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'getCurrentRound', data, {headers: headers})
      .map(response => response.json())
      .catch((err) => {
        this.toastr.error('Bad Request', 'Error');
        return Observable.throw(err);
    });
  }
  getNextRound(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'myTeam/getNextRound', data, {headers: headers})
      .map(response => response.json())
      .catch((err) => {
        this.toastr.error('Bad Request', 'Error');
        return Observable.throw(err);
    });
  }

  getMatchesByRounds(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/getMatchesByRounds', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to add round', 'Error');
        return Observable.throw(err);
    });
  }

  getAllPoints(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'getAllPoints', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to add round', 'Error');
        return Observable.throw(err);
    });
  }
}


