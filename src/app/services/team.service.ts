import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { API } from '../app.constants';
import { Player } from '../models/Player';
import { Formation } from '../models/Formation';
import { FieldPosition } from '../models/FieldPosition';
import * as _ from 'lodash';

@Injectable()
export class TeamService {
  availablePlayers: Array<Player>;
  selectedFormation: Formation;
  formations: Array<Formation>;
  substitutions = [];
  selectedTeam: Array<FieldPosition> = [];

  defPositions = ['CB', 'CBL', 'CBR', 'LB', 'RB'];
  midPositions = ['CMF', 'CMFL', 'CMFR', 'LMF', 'RMF', 'AMF'];
  forwardPositions = ['CF', 'CFL', 'CFR', 'LWF', 'RWF'];

  constructor(
    private http: Http,
    private toastr: ToastrService
  ) {
    this.formations = [
      {
        name: '4-4-2',
        positions: ['CBL', 'CBR', 'LB', 'RB', 'CMFL', 'CMFR', 'LMF', 'RMF', 'CFL', 'CFR']
      },
      {
        name: '4-3-3',
        positions: ['CBL', 'CBR', 'LB', 'RB', 'CMFL', 'CMFR', 'AMF', 'LWF', 'RWF', 'CF']
      },
      {
        name: '4-5-1',
        positions: ['CBL', 'CBR', 'LB', 'RB', 'CMFL', 'CMFR', 'LMF', 'RMF', 'AMF', 'CF']
      },
      {
        name: '3-5-2',
        positions: ['CB', 'LB', 'RB', 'CMFL', 'CMFR', 'LMF', 'RMF', 'AMF', 'CFL', 'CFR']
      },
      {
        name: '3-4-3',
        positions: ['CB', 'LB', 'RB', 'CMFL', 'CMFR', 'LMF', 'RMF', 'LWF', 'RWF', 'CF']
      },
      {
        name: '5-4-1',
        positions: ['CB', 'CBL', 'CBR', 'LB', 'RB', 'CMFL', 'CMFR', 'LMF', 'RMF', 'CF']
      },
      {
        name: '5-3-2',
        positions: ['CB', 'CBL', 'CBR', 'LB', 'RB', 'CMFL', 'CMFR', 'AMF', 'CFL', 'CFR']
      }
    ];
  }

  getMyTeam(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'myTeam/getMyTeamPage', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get my team', 'Error');
        return Observable.throw(err);
      });
  }

  getPlayers(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(API + 'players/getPlayers', {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get clubs', 'Error');
        return Observable.throw(err);
      });
  }

  getClubs(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'myTeam/getClubs', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get clubs', 'Error');
        return Observable.throw(err);
      });
  }

  checkTransfers(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'myTeam/checkTransfer', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get transfers!', 'Error');
        return Observable.throw(err);
      });
  }

  makeTransfer(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'myTeam/makeTransfer', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to make transfer!', 'Error');
        return Observable.throw(err);
      });
  }

  getPrivateLeagues(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'privateLeague/getPrivateLeagues', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get private leagues!', 'Error');
        return Observable.throw(err);
      });
  }
  // Za private league
  getLeaguePlayers(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'privateLeague/showTable', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get this leagues players!', 'Error');
        return Observable.throw(err);
      });
  }
  banUserFromPrivateLeague(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'privateLeague/banUser', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to ban this user!', 'Error');
        return Observable.throw(err);
      });
  }
  deletePrivateLeague(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'privateLeague/deleteLeague', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('You must kick all players first', 'Error');
        return Observable.throw(err);
      });
  }
  joinPrivateLeague(data) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'privateLeague/joinLeague', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to join this private league', 'Error');
        return Observable.throw(err);
      });
  }
  leavePrivateLeague(data) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'privateLeague/leaveLeague', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to leave this league', 'Error');
        return Observable.throw(err);
      });
  }
  createLeague(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http
        .post(API + 'privateLeague/createLeague', data, {headers: headers})
        .map(response => response.json())
        .catch((err) => {
          this.toastr.error('Bad Request', 'Error');
          return Observable.throw(err);
      });
    }
}
