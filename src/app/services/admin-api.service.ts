import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { API } from '../app.constants';

@Injectable()
export class AdminApiService {
  selectedLeague: number;
  formData: FormData;
  constructor(
    private http: Http,
    private toastr: ToastrService
  ) {
    const localStorageLeague = localStorage.getItem('ff-league');

    this.selectedLeague = localStorageLeague ?  JSON.parse(localStorage.getItem('ff-league')) : null;
  }

// admin login
adminLogin(data): Observable<any> {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');

  return this.http.post(API + 'user/login', data, {headers: headers})
    .map((res: Response) => res.json())
    .catch((err) => {
      this.toastr.error('Wrong Credentials', 'Error');
      return Observable.throw(err);
    });
}
// clubs
  getClubs(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/getClubs', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get clubs', 'Error');
        return Observable.throw(err);
      });
  }

  postClub(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/postClub', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to post club', 'Error');
        return Observable.throw(err);
      });
  }
  editClub(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(API + 'admin/updateClub', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to edit club', 'Error');
        return Observable.throw(err);
      });
  }

  removeClub(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/removeClub', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to post club', 'Error');
        return Observable.throw(err);
      });
  }

  getPlayersByClub(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/getPlayersByClub', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to get players from clubs', 'Error');
        return Observable.throw(err);
      });
  }
  // news
  getNews(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
               .get(API + 'admin/getArticles', {headers: headers})
               .map(response => response.json())
               .catch((err) => {
                 this.toastr.error('Bad Request', 'Error');
                 return Observable.throw(err);
               });
  }

  postNews(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/postArticle', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to post news', 'Error');
        return Observable.throw(err);
      });
  }

  uploadImage(file): Observable<any> {
    const headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    this.formData = new FormData();
    this.formData.append('file', file);
    headers.append('Authorization', `Bearer ${localStorage.getItem('ff-token')}`);
    return this.http.post(API + 'saveImage', this.formData, {headers: headers})
    .map((res: Response) => res.json())
    .catch((err) => {
      this.toastr.error('Unable to send image', 'Error');
      return Observable.throw(err);
      });
    }

  editNews(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(API + 'admin/updateArticle', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to edit news', 'Error');
        return Observable.throw(err);
      });
  }
  removeNews(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/removeArticle', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to remove club', 'Error');
        return Observable.throw(err);
      });
  }
// leagues
getLeague(data): Observable<any>  {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return this.http.post(API + 'admin/getLeague', data, {headers: headers})
    .map((res: Response) => res.json())
    .catch((err) => {
      this.toastr.error('Unable to add League', 'Error');
      return Observable.throw(err);
    });
}

getLeagues(): Observable<any> {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');

  return this.http
             .get(API + 'admin/getLeagues', {headers: headers})
             .map(response => response.json())
             .catch((err) => {
               this.toastr.error('Bad Request', 'Error');
               return Observable.throw(err);
             });
          }

  addLeague(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/postLeague', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to add League', 'Error');
        return Observable.throw(err);
      });
  }
  editLeague(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(API + 'admin/updateLeague', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to edit League', 'Error');
        return Observable.throw(err);
      });
  }
  removeLeague(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/removeLeague', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to remove League', 'Error');
        return Observable.throw(err);
      });
  }
  // seasons
  getSeasons(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
               .get(API + 'admin/getSeasons', {headers: headers})
               .map(response => response.json())
               .catch((err) => {
                 this.toastr.error('Bad Request', 'Error');
                 return Observable.throw(err);
               });
            }
    addSeason(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/postSeason', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to add Season', 'Error');
          return Observable.throw(err);
        });
    }
    removeSeason(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/removeSeason', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to remove Season', 'Error');
          return Observable.throw(err);
        });
    }
    // rounds
    getRounds(data): Observable<any>{
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/getRounds', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to add round', 'Error');
          return Observable.throw(err);
        });
    }
  getMatchesByRounds(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/getMatchesByRounds', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to add round', 'Error');
        return Observable.throw(err);
      });
    }
    postRound(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/postRound', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to add round', 'Error');
          return Observable.throw(err);
        });
    }
    postRoundDeadline(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.put(API + 'admin/setDeadline', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to set deadline for this round', 'Error');
          return Observable.throw(err);
        });
    }
    removeRound(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/removeRound', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to remove round', 'Error');
          return Observable.throw(err);
        });
    }
       // match

  getMatches(data): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(API + 'admin/getMatches', data, {headers: headers})
      .map((res: Response) => res.json())
      .catch((err) => {
        this.toastr.error('Unable to add match', 'Error');
        return Observable.throw(err);
      });
  }
    postMatch(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/postMatch', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to add match', 'Error');
          return Observable.throw(err);
        });
    }
    editMatch(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/updateMatch', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to edit match', 'Error');
          return Observable.throw(err);
        });
    }
    removeMatch(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/removeMatch', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to remove match', 'Error');
          return Observable.throw(err);
        });
    }
    // player

    getPlayers(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/getPlayers', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to get players', 'Error');
          return Observable.throw(err);
        });
    }
      postPlayer(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(API + 'admin/postPlayer', data, {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to add player', 'Error');
            return Observable.throw(err);
          });
      }
      editPlayer(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put(API + 'admin/updatePlayer', data, {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to add edit', 'Error');
            return Observable.throw(err);
          });
      }
      removePlayer(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(API + 'admin/removePlayer', data, {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to remove player', 'Error');
            return Observable.throw(err);
          });
      }

          // USER

    getUsers(data): Observable<any> {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API + 'admin/getUsers', data, {headers: headers})
        .map((res: Response) => res.json())
        .catch((err) => {
          this.toastr.error('Unable to add user', 'Error');
          return Observable.throw(err);
        });
    }
      postUser(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(API + 'admin/postUser', data, {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to add user', 'Error');
            return Observable.throw(err);
          });
      }
      editUser(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put(API + 'admin/updateUser', data, {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to add user', 'Error');
            return Observable.throw(err);
          });
      }

      removeUser(data): Observable<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(API + 'admin/removeUser', data, {headers: headers})
          .map((res: Response) => res.json())
          .catch((err) => {
            this.toastr.error('Unable to remove user', 'Error');
            return Observable.throw(err);
          });
      }
      // Squad

  getClubsByMatch(data): Observable<any> {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return this.http.post(API + 'admin/getPlayersStats', data, {headers: headers})
    .map((res: Response) => res.json())
    .catch((err) => {
      this.toastr.error('Unable to get clubs', 'Error');
      return Observable.throw(err);
    });
}
postPlayerStats(data): Observable<any> {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return this.http.post(API + 'admin/postPlayerStats', data, {headers: headers})
    .map((res: Response) => res.json())
    .catch((err) => {
      this.toastr.error('Unable to send players statistic', 'Error');
      return Observable.throw(err);
    });
}
// image

// uploadImage(file): Observable<any> {
//   const headers = new Headers();
//   headers.append('Content-Type', 'application/json');
//   this.formData = new FormData();
//   this.formData.append('image', file);
//   return this.http.post(API + 'admin/postArticle', this.formData, {headers: headers})
//   .map((res: Response) => res.json())
//   .catch((err) => {
//     this.toastr.error('Unable to send image', 'Error');
//     return Observable.throw(err);
//     });
//   }
}

